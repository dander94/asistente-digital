################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../dpl/ClockP_freertos.c \
../dpl/DebugP_freertos.c \
../dpl/HwiPMSP432_freertos.c \
../dpl/MutexP_freertos.c \
../dpl/PowerMSP432_freertos.c \
../dpl/SemaphoreP_freertos.c \
../dpl/SystemP_freertos.c 

C_DEPS += \
./dpl/ClockP_freertos.d \
./dpl/DebugP_freertos.d \
./dpl/HwiPMSP432_freertos.d \
./dpl/MutexP_freertos.d \
./dpl/PowerMSP432_freertos.d \
./dpl/SemaphoreP_freertos.d \
./dpl/SystemP_freertos.d 

OBJS += \
./dpl/ClockP_freertos.obj \
./dpl/DebugP_freertos.obj \
./dpl/HwiPMSP432_freertos.obj \
./dpl/MutexP_freertos.obj \
./dpl/PowerMSP432_freertos.obj \
./dpl/SemaphoreP_freertos.obj \
./dpl/SystemP_freertos.obj 

OBJS__QUOTED += \
"dpl\ClockP_freertos.obj" \
"dpl\DebugP_freertos.obj" \
"dpl\HwiPMSP432_freertos.obj" \
"dpl\MutexP_freertos.obj" \
"dpl\PowerMSP432_freertos.obj" \
"dpl\SemaphoreP_freertos.obj" \
"dpl\SystemP_freertos.obj" 

C_DEPS__QUOTED += \
"dpl\ClockP_freertos.d" \
"dpl\DebugP_freertos.d" \
"dpl\HwiPMSP432_freertos.d" \
"dpl\MutexP_freertos.d" \
"dpl\PowerMSP432_freertos.d" \
"dpl\SemaphoreP_freertos.d" \
"dpl\SystemP_freertos.d" 

C_SRCS__QUOTED += \
"../dpl/ClockP_freertos.c" \
"../dpl/DebugP_freertos.c" \
"../dpl/HwiPMSP432_freertos.c" \
"../dpl/MutexP_freertos.c" \
"../dpl/PowerMSP432_freertos.c" \
"../dpl/SemaphoreP_freertos.c" \
"../dpl/SystemP_freertos.c" 


