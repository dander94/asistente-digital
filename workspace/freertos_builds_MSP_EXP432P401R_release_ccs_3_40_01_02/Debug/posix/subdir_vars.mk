################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../posix/PTLS.c \
../posix/aeabi_portable.c \
../posix/clock.c \
../posix/memory.c \
../posix/mqueue.c \
../posix/pthread.c \
../posix/pthread_barrier.c \
../posix/pthread_cond.c \
../posix/pthread_mutex.c \
../posix/pthread_rwlock.c \
../posix/sched.c \
../posix/semaphore.c \
../posix/sleep.c \
../posix/timer.c 

C_DEPS += \
./posix/PTLS.d \
./posix/aeabi_portable.d \
./posix/clock.d \
./posix/memory.d \
./posix/mqueue.d \
./posix/pthread.d \
./posix/pthread_barrier.d \
./posix/pthread_cond.d \
./posix/pthread_mutex.d \
./posix/pthread_rwlock.d \
./posix/sched.d \
./posix/semaphore.d \
./posix/sleep.d \
./posix/timer.d 

OBJS += \
./posix/PTLS.obj \
./posix/aeabi_portable.obj \
./posix/clock.obj \
./posix/memory.obj \
./posix/mqueue.obj \
./posix/pthread.obj \
./posix/pthread_barrier.obj \
./posix/pthread_cond.obj \
./posix/pthread_mutex.obj \
./posix/pthread_rwlock.obj \
./posix/sched.obj \
./posix/semaphore.obj \
./posix/sleep.obj \
./posix/timer.obj 

OBJS__QUOTED += \
"posix\PTLS.obj" \
"posix\aeabi_portable.obj" \
"posix\clock.obj" \
"posix\memory.obj" \
"posix\mqueue.obj" \
"posix\pthread.obj" \
"posix\pthread_barrier.obj" \
"posix\pthread_cond.obj" \
"posix\pthread_mutex.obj" \
"posix\pthread_rwlock.obj" \
"posix\sched.obj" \
"posix\semaphore.obj" \
"posix\sleep.obj" \
"posix\timer.obj" 

C_DEPS__QUOTED += \
"posix\PTLS.d" \
"posix\aeabi_portable.d" \
"posix\clock.d" \
"posix\memory.d" \
"posix\mqueue.d" \
"posix\pthread.d" \
"posix\pthread_barrier.d" \
"posix\pthread_cond.d" \
"posix\pthread_mutex.d" \
"posix\pthread_rwlock.d" \
"posix\sched.d" \
"posix\semaphore.d" \
"posix\sleep.d" \
"posix\timer.d" 

C_SRCS__QUOTED += \
"../posix/PTLS.c" \
"../posix/aeabi_portable.c" \
"../posix/clock.c" \
"../posix/memory.c" \
"../posix/mqueue.c" \
"../posix/pthread.c" \
"../posix/pthread_barrier.c" \
"../posix/pthread_cond.c" \
"../posix/pthread_mutex.c" \
"../posix/pthread_rwlock.c" \
"../posix/sched.c" \
"../posix/semaphore.c" \
"../posix/sleep.c" \
"../posix/timer.c" 


