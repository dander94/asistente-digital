/*
 * sync_code.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene el hilo de sync
 */
#include <stdio.h>
#include <stdint.h>


/* MSP432 SDK */
#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>

/* RTOS */
#include "rtos/rtos.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* MQTT */
#include "mqtt_client/mqtt_client.h"

/* Pantalla */
#include "pantalla/pantalla.h"

/* Console */
#include "console_client/console_client.h"

#define SYNC_CODE_TIMEOUT_MS 15000

/* Fatal Error */
extern void fatal_error(uint8_t error_code, char * msg);

/* Variables globales */
static char sync_code[SYNC_CODE_LEN + 1];

/*
 * Hilo de sync code. Esperara hasta que el sistema este conectado, luego habilitara el boton y esperara hasta que se
 * mantenga pulsado el boton de sync durante 5 segundos
 */
void *syncCodeThread(void *arg0) {
    // Esperamos a que el sistema este en funcionamiento
    while(CONFIG_get_system_status() != _SYS_READY)  {
        task_delay(50);
    }

    // Habilitamos el boton de sync code.
    CONFIG_enable_sync_button();

    while (sem_wait(&sync_code_semaphore) == 0) {
        // No har� nada hasta que no se solicite el sync_code
        // Deshabilitamos las interrupciones para pedir sync
        MAP_Interrupt_disableInterrupt(INT_TA2_0);
        MAP_Interrupt_disableInterrupt(INT_PORT5);

        LCD_print_start_sync_mode();

        // Enviamos el mensaje a la cola de publicaci�n
        if(!add_publish_queue_message(_MQTT_MSG_SYNC_REQ, "", 0, 5000)) {
            fatal_error(1, "Couldnt ask sync code");
        }

        // Esperamos a recibir el sync code
        if(read_sync_code_queue(sync_code, SYNC_CODE_TIMEOUT_MS) != SYNC_CODE_LEN + 1) {
            fatal_error(11, "Sync Code Never Received");
        }

        // Una vez recibido lo mostramos en pantalla.
        LCD_print_sync_code(sync_code);


        // Habilitamos las interrupciones de nuevo
        MAP_Interrupt_enableInterrupt(INT_TA2_0);
        MAP_Interrupt_enableInterrupt(INT_PORT5);
    }

    while(1){}
}
