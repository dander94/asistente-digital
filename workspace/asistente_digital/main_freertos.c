/*
 * main_freertos.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero es el punto de entrada de la aplicacion. Aqui se crearan los threads, se definiran variables
 * y funciones globales.
 */

#include <stdint.h>
#include <stdio.h>

/* MSP432 SDK */
#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>

/* POSIX */
#include <pthread.h>
#include <mqueue.h>

/* RTOS  */
#include <FreeRTOS.h>
#include <task.h>

/* GPIO */
#include <ti/drivers/GPIO.h>

/* Configuracion Drivers */
#include <ti/drivers/Board.h>

/* Libreria propia de RTOS */
#include "rtos/rtos.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* Consola */
#include "console_client/console_client.h"

/* Pantalla */
#include "pantalla/pantalla.h"

/* Buzzer */
#include "sonido/buzzer.h"


/* Tamanyo de la pila y prioridades*/
#define SUBSCRIPTION_STACK_SIZE     4096
#define SUBSCRIPTION_PRIORITY       4

#define PUBLISH_STACK_SIZE          4096
#define PUBLISH_PRIORITY            6

#define SYNC_STACK_SIZE             1024
#define SYNC_PRIORITY               1

#define NOTIF_STACK_SIZE            1024
#define NOTIF_PRIORITY              5

#define SENSORES_STACK_SIZE         1024
#define SENSORES_PRIORITY           2

#define HEARTB_STACK_SIZE           1024
#define HEARTB_PRIORITY             3

/*--------------------------*/

/*  Hilos */
extern void *publishThread(void *arg0);
extern void *subscriptionThread(void *arg0);
extern void *syncCodeThread(void *arg0);
extern void *notificationJoystickThread(void *arg0);
extern void *sensoresThread(void *arg0);
extern void *heartbThread(void *arg0);
/*--------------------------*/


/*
 * Funcion auxiliar para crear un thread
 * @param thread. Puntero al phthread_t del hilo a crear.
 * @param attrs. Puntero al pthread_attr_t del hilo a crear.
 * @param priority_obj. Puntero al objeto sched_param donde almacenar la prioridad del hilo a crear.
 * @param priority. Prioridad del hilo
 * @param stack_size. Tama�o de la pila.
 * @param handler. Funcion del hilo
 * @return 0 si se ha creado correctamente. 1 si no se ha podido inicializar. 2 si no se ha podido crear.
 */
static uint8_t create_thread(pthread_t *thread, pthread_attr_t *attrs, struct sched_param  *priority_obj, uint8_t priority, uint16_t stack_size, void * handler(void *)) {
    int8_t res;

    // Inicializamos los atributos del hilo
    pthread_attr_init(attrs);

    // Anyadimos los parametros del hilo como prioridad o tamanyo de pila
    res = pthread_attr_setschedparam(attrs, priority_obj);
    res &= pthread_attr_setdetachstate(attrs, PTHREAD_CREATE_DETACHED);
    res &= pthread_attr_setstacksize(attrs, stack_size);

    if (res != 0) {
        // Fallo al inicializar
        return 1;
    }

    // Creamos el hilo
    res = pthread_create(thread, attrs, handler, NULL);

    if (res != 0) {
        // No se ha podido crear el hilo
        return 2;
    }
    return res;
}

/*
 * Fatal Error. Esta funcion detendr� la ejecuci�n y realizar� determinadas acciones en funcion del codigo de error
 * @param error_code. Codigo interno de error
 * @param console_message. Mensaje que se imprimira por consola
 */
void fatal_error(uint8_t error_code, char * console_message) {
    char err_code[13];

    // 0 -> Error del Dispositivo
    // 10 -> Credenciales del Wifi invalidas
    // 11 -> Error de servidor

    console_write("*-Fatal Error-*");
    console_write(console_message);
    console_write("*-------------*");

    switch (error_code) {
        case 10:  // INVALID CREDENTIALS
            LCD_fatal_error("WIFI ERROR", "Invalid Credentials");
            break;
        case 11: // SERVER DOWN
            LCD_fatal_error("FATAL ERROR", "Server Down!");
            break;
        default:
            sprintf(err_code, "Error Code: %d", error_code);
            LCD_fatal_error("FATAL ERROR", err_code);
    }

    BUZZER_play_error_song(DEVICE_VOLUME);

    // Paramos el scheduler
    vTaskEndScheduler();

    // Detenemos la ejecucion
    while(1);
}


/*
 * main
 */
int main(void)
{
    pthread_t           pub_thread;
    pthread_t           sync_thread;
    pthread_t           notif_thread;
    pthread_t           sub_thread;
    pthread_t           sensors_thread;
    pthread_t           heartb_thread;

    pthread_attr_t      p_attrs;
    struct sched_param  p_priority;

    int8_t res;

    // Inicializamos la placa. Establecer� la frecuencia DCO a 48MHz, SMCLK a 12MHz y ACKL a 32Khz
    Board_init();

    // Inicializamos GPIO
    GPIO_init();

    // Inicializamos la consola
    init_console();

    // Inicializamos la pantalla
    LCD_init();

    // Inicializamos el buzzer
    BUZZER_init();

    // Inicializamos las colas
    init_queues();

    // Inicializamos la configuracion
    if (CONFIG_init() != 1) {
        fatal_error(0, "Config Init");
    }

    /* Creacion de hilos */

    // Suscripcion
    res = create_thread(&sub_thread, &p_attrs, &p_priority, SUBSCRIPTION_PRIORITY, SUBSCRIPTION_STACK_SIZE, subscriptionThread);

    if (res > 0) {
        fatal_error(0, "Create Subscription Thread");
    }

    // Publicacion
    res = create_thread(&pub_thread, &p_attrs, &p_priority, PUBLISH_PRIORITY, PUBLISH_STACK_SIZE, publishThread);

    if (res > 0) {
        fatal_error(0, "Create Publish Thread");
    }

    // Sync Code
    res = create_thread(&sync_thread, &p_attrs, &p_priority, SYNC_PRIORITY, SYNC_STACK_SIZE, syncCodeThread);

    if (res > 0) {
        fatal_error(0, "Create Sync Thread");
    }

    // Notif
    res = create_thread(&notif_thread, &p_attrs, &p_priority, NOTIF_PRIORITY, NOTIF_STACK_SIZE, notificationJoystickThread);

    if (res > 0) {
        fatal_error(0, "Create Notif Thread");
    }

    // Sensores
    res = create_thread(&sensors_thread, &p_attrs, &p_priority, SENSORES_PRIORITY, SENSORES_STACK_SIZE, sensoresThread);

    if (res > 0) {
        fatal_error(0, "Create Sensors Thread");
    }

    // Heartbeat
    res = create_thread(&heartb_thread, &p_attrs, &p_priority, HEARTB_PRIORITY, HEARTB_STACK_SIZE, heartbThread);

    if (res > 0) {
        fatal_error(0, "Create Heartbeat Thread");
    }

    // Habilitamos las interrupciones
    MAP_Interrupt_enableMaster();

    // Reproducimos el sonido de bienvenida
    BUZZER_play_welcome_song(DEVICE_VOLUME);

    // Iniciamos el scheduler
    vTaskStartScheduler();

    return (0);
}


/* La siguiente fraccion de codigo son los handlers por defecto para FreeRTOS. */


void vApplicationMallocFailedHook()
{
    // Error Malloc
    fatal_error(0, "Malloc Error");
}

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
    // Error desbordamiento de pila
    fatal_error(0, "Stack Overflow");
}
