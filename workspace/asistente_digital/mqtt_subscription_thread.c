/*
 * mqtt_subscription_thread.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene el hilo de subscripcion MQTT
 */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Configuracion */
#include "configuracion/configuracion.h"

/* Consola */
#include "console_client/console_client.h"

/* Cliente MQTT */
#include "mqtt_client/mqtt_client.h"

/* RTOS */
#include "rtos/rtos.h"

/* Pantalla */
#include "pantalla/pantalla.h"

/* Fatal Error */
extern void fatal_error(uint8_t error_code, char * msg);

uint8_t g_stop_mqtt_flag;

/*
 * Funcion auxiliar para detener el bucle de subscripcion
 */
void stop_mqtt_subscription() {
    g_stop_mqtt_flag = 1;
}

/*
 * Funcion auxiliar para determinar el tipo de mensaje.
 * @param topic.
 * @return MQTTRecMsg_t Tipo de mensaje recibido
 */
static MQTTRecMsg_t find_msg_type(char * topic) {
    if (strstr(topic, MQTT_TOPIC_SYNC) != NULL) {
        return _MQTT_MSG_SYNC_RESP;
    } else if (strstr(topic, MQTT_TOPIC_CONF) != NULL) {
        return _MQTT_MSG_CONF_RESP;
    } else if (strstr(topic, MQTT_TOPIC_HEARTB) != NULL) {
        return _MQTT_MSG_HEARTB_RESP;
    } else {
        return _MQTT_MSG_INVALID_RESP;
    }
}

/*
 * Funcion para actuar en caso de conf rest. Enviara la configuracion de los sensores al hilo de sensores y almacenara la configuracion wifi.
 * @param payload . Mensaje procedente de MQTT
 */
static void handle_conf_resp(char * payload) {
    // 0;00;00
    char sensors_msg[8] = {'\0'};

    // Contenedor auxiliar para la conversion de la longitud.
    char aux_len[3] = {'\0'};

    // Variables para el SSID y para la PASS.
    char ssid[SSID_MAX_LEN + 1] = {'\0'};
    char pass[PASS_MAX_LEN + 1] = {'\0'};

    // Longitud del SSID. Longtid de la contrasenya. Variable para bucle.
    uint8_t ssid_len;
    uint8_t pass_len;

    // Recuperamos la parte de los sensores
    memcpy(sensors_msg, payload, 7);

    // Lo enviamos al hilo de sensores
    add_sensors_queue_message(sensors_msg, 5000);

    // Copiamos la longitud recibidar del ssid y la convertirmos a entero. Lo mismo con la pass.
    memcpy(aux_len, payload + 8, 2);
    ssid_len = atoi(aux_len);
    memcpy(aux_len, payload + 11, 2);
    pass_len = atoi(aux_len);

    // Copiamos el ssid
    if (ssid_len > 0) {
        memcpy(ssid, payload + 14, ssid_len);
    }

    // Copiamos la contranseya
    if (pass_len > 0 ) {
        memcpy(pass, payload + 14 + ssid_len + 1, pass_len);
    }

    // Guardamos el ssid y la contrase�a, incluso si estan vacios
    CONFIG_save_wifi(ssid, ssid_len, pass, pass_len);
}

/*
 * Callback para cuando se reciba un mensaje en MQTT
 * @param topic
 * @param payload
 */
void on_msg(char * topic, char * payload) {
    // Imprimimos por la consola el mensaje recibido
    console_write("-RECEIVED-");
    console_write(topic);
    console_write(payload);
    console_write("----------");

    switch (find_msg_type(topic)) {
        case _MQTT_MSG_SYNC_RESP:
            // Si es un mensaje de respuesta de sync, se lo enviamos al hilo de sync
            add_sync_code_queue(payload, 5000);
            break;
        case _MQTT_MSG_CONF_RESP:
            handle_conf_resp(payload);
            break;
        case _MQTT_MSG_HEARTB_RESP:
            add_heartb_queue_message("", 5000);
            break;
        case _MQTT_MSG_INVALID_RESP:
        default:
            break;
    }
}

/*
 * _Hilo de subscrpcion. Iniciara la conexti�n MQTT, el bucle de subscripcion y realizara una accion segun el mensaje recibido.
 */
void *subscriptionThread(void *arg0) {
    uint8_t res;
    char ssid[SSID_MAX_LEN + 1] = {'\0'};
    char pass[PASS_MAX_LEN + 1] = {'\0'};

    LCD_connecting();

    CONFIG_read_wifi(ssid, pass);

    console_write("Iniciando conexion...");
    console_write(ssid);
    console_write(pass);
    // Nos conectamos al broker MQTT
    res = MQTT_connect(ssid, pass);

    switch (res) {
        case MQTT_RET_CONNECT_OK:
            console_write("MQTT Conectado");
            break;
        case MQTT_RET_ERR_INIT:
        case MQTT_RET_ERR_CONNACK:
        case MQTT_RET_ERR_TCP:
        case MQTT_RET_ERR_TLS:
            fatal_error(11, "MQTT Down!");
            break;
        case MQTT_RET_ERR_ESP:
            fatal_error(0, "ESP Error");
        case MQTT_RET_ERR_AP:
            fatal_error(10, "Wifi: Invalid Credentials");
        default:
            fatal_error(0, "Unknown MQTT RET");
            break;
    }

    // Cambiamos el estado a conectado
    CONFIG_set_system_status(_SYS_CONNECTED);

    // Nos suscribimos a los topics
    if(!MQTT_subscribe_all()) {
        fatal_error(11, "MQTT Subscribe Error");
    }

    // Iniciamos el loop de suscripcion
    MQTT_subscription_loop(on_msg, &g_stop_mqtt_flag);

    // Si se llega aqui, es que se debe para la subscripcion y cerrar la conexi�n
    MQTT_disconnect();

    while(1);
}
