/*
 * console_client.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad para trabajar con la UART como consola
 */

#include "console_client.h"

#include <string.h>

// Importamos el driver de UART. Usaremos los drivers generales que proporcionan.
#include <ti/drivers/UART.h>

// Importamos la configuraci�n generada por syscfg
#include "ti_drivers_config.h"


/* Variables globales */
UART_Handle uart_handle;
/*--------------------*/

void console_write(const char * message) {
    UART_write(uart_handle, message, strlen(message));
    UART_write(uart_handle, "\r\n", 2);
}

void init_console() {
    UART_Params uartParams;

    // Iniciamos la UART
    UART_init();
    
    // Configuramos los parametros. Lectura/escritura binaria. Return full. Sin Echo. Y Baud Rate 115200
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 115200;
    
    // Abrimos la consola para poder utilizarla
    uart_handle = UART_open(CONFIG_UART_0, &uartParams);
}
