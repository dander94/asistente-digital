/*
 * mqtt_publish_thread.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene el hilo de publicacion MQTT
 */

#include <stdint.h>
#include <string.h>

/* POSIX */
#include <mqueue.h>

/* RTOS */
#include "rtos/rtos.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* Consola */
#include "console_client/console_client.h"

/* Cliente MQTT */
#include "mqtt_client/mqtt_client.h"

/* Fatal Error */
extern void fatal_error(uint8_t error_code, char * msg);

/*
 * Hilo de publish. Este hilo recibira mensajes para publicar de una cola y segun su tipo, hara una accion concreta
 */
void *publishThread(void *arg0)
{
    char rec_buffer[PUB_Q_MSG_SIZE];
    int8_t msg_size;

    while(CONFIG_get_system_status() != _SYS_CONNECTED) {
        // No se hara nada hasta que no se haya conectado con MQTT.
        task_delay(50);
    }

    while(1) {
        // Leemos un mensaje de la cola, con un timeout de 200.
        msg_size = read_publish_queue(rec_buffer, 200);

        if (msg_size > 0) {
            // Mensaje recibido. Miraremos la primera posicion de la string para saber el tipo de mensaje
            switch ((MQTTPubMsg_t) rec_buffer[0]) {
                case _MQTT_MSG_SYNC_REQ:
                    // Pedimos el Sync Code
                    MQTT_ask_sync_code();
                    break;
                case _MQTT_MSG_CONF_REQ:
                    // Pedimos la configuracion
                    MQTT_ask_conf();
                    break;
                case _MQTT_MSG_NOTIF:
                    // Publicamos la notificacion
                    if (!MQTT_publish_notification(rec_buffer + 1)) {
                        // Esperamos y se intenta una segunda vez
                        task_delay(2000);
                        if (!MQTT_publish_notification(rec_buffer + 1)) {
                            fatal_error(11, "Publish Notification Error");
                        }
                    }
                    break;
                case _MQTT_MSG_HEARTB:
                    MQTT_publish_heartb();
                    break;
                default:
                    break;
            }
            rec_buffer[0] = 0;
        }
    }
}

