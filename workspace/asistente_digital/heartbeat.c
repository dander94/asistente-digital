/*
 * heartbeat.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene el hilo de heartbeat. Inciara la conexion con el broker MQTT y enviara cada cierto tiempo una se�al de HEartbeat. Asi mismo, esperara
 * la respuesta del servidor para determinar que se tiene conexion con este durante toda la ejecucion.
 */

#include <stdint.h>
#include <string.h>

/* POSIX */
#include <mqueue.h>

/* RTOS */
#include "rtos/rtos.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* Consola */
#include "console_client/console_client.h"

/* Buzzer */
#include "sonido/buzzer.h"

/* Pantalla */
#include "pantalla/pantalla.h"

/* Cliente MQTT */
#include "mqtt_client/mqtt_client.h"

/* Fatal Error */
extern void fatal_error(uint8_t error_code, char * msg);

/*
 * Hilo de hearbeat. Enviar� un heartbeat al servidor y esperara la respuesta de este
 */
void *heartbThread(void *arg0)
{
    uint16_t heartb_cycles = 0;
    uint16_t no_heartb_resp_cycles = 0;

    while(!CONFIG_get_system_status() == _SYS_CONNECTED) {
       task_delay(100);
    }

    // Enviamos el primer heartb
    add_publish_queue_message(_MQTT_MSG_HEARTB, "", 0, 5000);

    while(1) {
        if (heartb_cycles++ == HEARTB_SECONDS) {
            // Enviamos el heartb
            add_publish_queue_message(_MQTT_MSG_HEARTB, "", 0, 5000);
            heartb_cycles = 0;
        }

        // Esperamos un segundo
        task_delay(1000);

        if (read_heartb_queue("", 0)) {
            // Respuesta recibida. Si no se ha hecho aun, se establece que el sistema esta listo
            if (CONFIG_get_system_status() == _SYS_CONNECTED) {
                CONFIG_set_system_status(_SYS_READY);
                BUZZER_play_connected_song(DEVICE_VOLUME);
                LCD_welcome();
            }
            no_heartb_resp_cycles = 0;
        } else {
            // Se suman ciclos sin respuesta
            if (++no_heartb_resp_cycles == HEARTB_RESP_MAX_SECONDS) {
                fatal_error(11, "Heartbeat Error");
            }
        }
    }
}

