/*
 * sensores.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene el hilo de sensores
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* RTOS */
#include "rtos/rtos.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* MQTT */
#include "mqtt_client/mqtt_client.h"

/* Ambiente */
#include "ambiente/ambiente.h"

/* Console */
#include "console_client/console_client.h"

/* Fatal Error */
extern void fatal_error(uint8_t error_code, char * msg);


/*
 * Hilo de sensores. Esperara hasta que el sistema este conectado, luego inicializara los sensores, pedira la configuracion mas reciente, y comprobara los sensores cada
 * cierto tiempo
 */
void *sensoresThread(void *arg0) {
    char aux_substring_buffer[3];
    uint16_t cycles_completed = 0; // Cuenta los ciclos completados sin haber comprobado los sensores de temperatura
    int16_t light_cycles_completed = 0; // Cuenta los ciclos que la luz se ha mantenido encendida durante el modo noche
    char conf[SENSORS_CONF_LEN + 1]; // Buffer para guardar la configuracion
    uint8_t new_conf_flag = 0; // Se pondra a 1 cuando se deban comprobar los sensores por una nueva notificacion


    // Esperamos a que el sistema este en funcionamiento
    while(CONFIG_get_system_status() != _SYS_READY)  {
        task_delay(50);
    }

    // Iniciamos los sensores
    SENSORS_init();

    // Pedimos la configuracion m�s reciente
    if(!add_publish_queue_message(_MQTT_MSG_CONF_REQ, "", 0, 5000)) {
        fatal_error(11, "Configuration never received");
        while(1);
    }

    do {
        // Leemos la cola de los sensores esperando una nueva configuracion
        if (read_sensors_queue(conf, 100) > 0) {
            // Establecemos si el modo nocturno esta activo
            SENSORS_set_night_mode(conf[0] == '1');
            // Establecemos la temperatura minima
            memcpy(aux_substring_buffer, ((char *) conf) + 2, 2);
            SENSORS_set_min_temperature(atoi(aux_substring_buffer));
            // Establecemos la temperatura maxima
            memcpy(aux_substring_buffer, ((char *) conf) + 5, 2);
            SENSORS_set_max_temperature(atoi(aux_substring_buffer));

            // Indicamos que la configuracion es nueva y se deben comprobar los sensores
            new_conf_flag  = 1;
        }

        // Se comprueba si hay nueva configuracion o el numero de ciclos transcurridos es el deseado
        if (new_conf_flag || cycles_completed == (TEMP_SENSOR_CHECK_MINUTES * 60)) {
            new_conf_flag = 0;
            cycles_completed = 0;

            // Se comprueban los sensores y se notifica si la temperatura es incorrecta
            if (!SENSORS_check_min_temperature()) {
                add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_TEMP_LOW, 2, 5000);
            } else if (!SENSORS_check_max_temperature()) {
                add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_TEMP_HIGH, 2, 5000);
            }
        } else {
            // Se anyade un ciclo
            cycles_completed++;
        }


        // A diferencia de la temperatura, la luz la comprobamos siempre
        if (new_conf_flag || !SENSORS_check_light()) {
            // Si la luz no es correcta, se comprobara si se no ha notificado la luz o han transcurrido los ciclos necesarios desde la ultima notificacion
            if (new_conf_flag || light_cycles_completed == 0 || (light_cycles_completed - 1) == (LIGHT_SENSOR_CHECK_MINUTES * 60)) {
                add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_LIGHT, 2, 5000);
                light_cycles_completed = 0;
            }
            light_cycles_completed++;
        } else if (light_cycles_completed > 0) {
            // Si la luz es correcta, los ciclos son cero. Ya no hay que notificar mas
            light_cycles_completed = 0;
        }
        task_delay(1000);
    } while(1);
}

