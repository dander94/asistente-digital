/*
 * mqtt_client.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene definiciones de tipos y funciones para MQTT
 */

#ifndef MQTT_CLIENT_MQTT_CLIENT_H_
#define MQTT_CLIENT_MQTT_CLIENT_H_

#include <stdint.h>

#define MQTT_BUFFER_SIZE     128

/* Definicion de retornos */
#define MQTT_RET_CONNECT_OK         0
#define MQTT_RET_ERR_ESP            1
#define MQTT_RET_ERR_AP             2
#define MQTT_RET_ERR_TLS            3
#define MQTT_RET_ERR_INIT           4
#define MQTT_RET_ERR_TCP            5
#define MQTT_RET_ERR_CONNACK        6

/* Tipo para publicar un mensaje */
typedef enum {
    _MQTT_MSG_SYNC_REQ, _MQTT_MSG_NOTIF, _MQTT_MSG_HEARTB, _MQTT_MSG_CONF_REQ, _MQTT_MSG_INVALID
} MQTTPubMsg_t;

/* Tipo para identificar un mensaje recibido */
typedef enum {
    _MQTT_MSG_SYNC_RESP, _MQTT_MSG_HEARTB_RESP, _MQTT_MSG_CONF_RESP, _MQTT_MSG_INVALID_RESP,
} MQTTRecMsg_t;

/*
 * Crea una conexion MQTT con el broker.
 * @param ssid del AP para establecer la conexion
 * @param password del AP para establecer la conexion
 * @return MQTT_RET_CONNECT_OK si se ha incializado correctamente. MQTT_RET_ERR_* si ha ocurrido algun error
 */
uint8_t MQTT_connect(char * ssid, char * password);

/*
 * Cierra la conexion MQTT con el broker.
 * @return 1 si se ha cerrado correctamente. 0 si ha fallado.
 */
uint8_t MQTT_disconnect();

/*
 * Funcion para subscribirse a los topics necesarios.
 * @return 1 si se ha suscrito correctamente. 0 si ha fallado.
 */
uint8_t MQTT_subscribe_all();

/*
 * Inicia un bucle indefinido para leer los mensajes publicados en topics suscritos.
 * @param on_message. Callback para la recepcion de un mensaje. Debe ser una Funcion que debe recibir un topic y un mensaje.
 * @param stop_flag Si es 1, el bucle parara
 */
void MQTT_subscription_loop(void on_message(char*, char *), uint8_t * stop_flag);

/*
 * Inicia un bucle indefinido para mantener la conexion activa
 * @param stop_flag Si es 1, el bucle parara y la funcion devolvera
 * @return 1 si se ha terminado mediante la stop flag. 0 si se ha terminado por un fallo en un ping
 */
uint8_t MQTT_ping_loop(uint8_t * stop_flag);

/*
 * Publica una peticion de sync code
 * @return 1 si se ha publicado. 0 si no.
 */
uint8_t MQTT_ask_sync_code();

/*
 * Metodo para publicar una notification
 * @param notificacion. Tipo de la notificacion
 * @return 1 si se ha publicado correctamente. 0 si no.
 */
uint8_t MQTT_publish_notification(char notification[3]);

/*
 * Publica una peticion de conf
 * @return 1 si se ha publicado correctamente. 0 si no.
 */
uint8_t MQTT_ask_conf();

/*
 * Publica un heartbeat
 * @return 1 si se ha publicado correctamente. 0 si no.
 */
uint8_t MQTT_publish_heartb();

#endif /* MQTT_CLIENT_MQTT_CLIENT_H_ */
