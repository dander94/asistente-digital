/*
 * tls.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef MQTT_CLIENT_TLS_H_
#define MQTT_CLIENT_TLS_H_

#include <stdint.h>

/* Codigos de Retorno de TLS INIT */
#define TLS_RET_OK                  0
#define TLS_RET_ERR_ADD_ENTROPY     1
#define TLS_RET_ERR_SEED            2
#define TLS_RET_ERR_CONFIG          3
#define TLS_RET_ERR_SETUP           4
#define TLS_RET_ERR_HOSTNAME        5

/*
 * Funcion para inicializar TLS
 * @return TLS_RET_OK si se ha iniciado correctamente. TLS_RET_ERR_* si ha ocurrido algun error.
 */
uint8_t TLS_init();

/*
 * Funcion para enviar datos a traves de SSL
 * @param buffer. Buffer con los datos a enviar
 * @param bytes. Bytes que se enviaran
 * @return bytes enviados.
 */
uint16_t TLS_send(char * buffer, uint16_t bytes);

/*
 * Funcion para recibir datos a traves de SSL
 * @param buffer. Buffer donde se almacenaran los datos
 * @param bytes. Bytes que se leeran
 * @return bytes leidos.
 */
uint16_t TLS_recv(char * buffer, uint16_t bytes);

#endif /* MQTT_CLIENT_TLS_H_ */
