/*
 * tls.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad para poder usar SSL en las conexiones con el servidor
 */

#include "tls.h"

#include <string.h>

/* mbedTLS PORT para MSP */
#include "mbedtls/port/entropy_alt.h"
#include "mbedtls/port/threading_alt.h"

/* mbedTLS */
#include "mbedtls/net.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/debug.h"
#include "mbedtls/error.h"

/* Cliente de ESP */
#include "esp_client.h"

/* Configuracion */
#include "configuracion/configuracion.h"


/* Variables de mbedTLS */
static mbedtls_entropy_context entropy;  // Contexto Entropia
static mbedtls_ctr_drbg_context ctr_drbg; // Contexto del Generador de Numeros Aleatorios
static mbedtls_ssl_context ssl; // Contexto SSL
static mbedtls_ssl_config conf; // Contexto de la configuracion de SSL
static size_t entropy_len;  // Variable para la longitud de la entropia

/*
 * Funcion interna para enviar datos a traves del ESP01 desde mbedTLS
 * @param ctx. Contexto de SSL
 * @param buff. Buffer con los datos a enviar
 * @param len. Longitud de los datos a enviar
 * @return bytes enviados
 */
static uint16_t tls_send(void *ctx, const unsigned char * buff, uint16_t len) {
    return ESP_tcp_send_bytes(buff, len);
}

/*
 * Funcion interna para recibir datos a traves del ESP01 desde mbedTLS
 * @param ctx. Contexto de SSL
 * @param buff. Buffer donde almacenar los datos a recibir
 * @param len. Longitud de los datos esperados
 * @return bytes leidos
 */
static uint16_t tls_recv(void *ctx, unsigned char * buff, uint16_t len) {
    return ESP_read_bytes(buff, len);
}

/*
 * Function interna. Callback de la funcion de entropia
 * @param data. Datos para la funcion.
 * @param output. Cadena donde almacenar el resultado
 * @param len. Longitud
 * @return 0 si se ha ejecutado correctamente
 */
static int32_t entropy_callback(void *data, unsigned char *output, size_t len) {
    return entropy_source(data, output, len, &entropy_len);
}

uint8_t TLS_init() {
    int8_t ret; // Variable para los return de mbedTLS
    char * seed = ASISTENTE_DEVICE_ID;  // Semilla. Usaremos la ID del dispositivo

    // Almacenamos la len de la entropia
    entropy_len = strlen(seed);

    // Iniciamos los contextos
    mbedtls_ssl_init(&ssl);
    mbedtls_ssl_config_init(&conf);
    mbedtls_ctr_drbg_init(&ctr_drbg);
    mbedtls_entropy_init(&entropy);

    // A�adimos la nueva funcion de threading (port)
    mbedtls_threading_set_alt(threading_mutex_init_pthread, threading_mutex_free_pthread, threading_mutex_lock_pthread, threading_mutex_unlock_pthread);

    // A�adimos la nueva funcion de entropia (port)
    ret = mbedtls_entropy_add_source(&entropy, entropy_source, NULL, 1, MBEDTLS_ENTROPY_SOURCE_STRONG);
    if (ret != 0) {
        return TLS_RET_ERR_ADD_ENTROPY;
    }

    // Establecemos la SEED del generador de numeros aleatorios
    ret = mbedtls_ctr_drbg_seed( &ctr_drbg, entropy_callback, &entropy, (const unsigned char *) seed, entropy_len);
    if(ret != 0)  {
        return TLS_RET_ERR_SEED;
    }

    // Establecemos la configuracion de SSL, indicando que es cliente, que utilizamos TCP y que el preset es el por defecto.
    ret = mbedtls_ssl_config_defaults(&conf, MBEDTLS_SSL_IS_CLIENT,MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);
    if (ret != 0) {
        return TLS_RET_ERR_CONFIG;
    }

    // Por el momento no se verificara el certificado.
    mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_NONE );

    // Configuramos el generador de numeros aleatorios
    mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);

    // Hacemos el setup de SSL con la configuracion
    ret = mbedtls_ssl_setup(&ssl, &conf);
    if (ret != 0) {
        return TLS_RET_ERR_SETUP;
    }

    // Ponemos como hostname la id del dispositivo
    ret = mbedtls_ssl_set_hostname(&ssl, ASISTENTE_DEVICE_ID);
    if (ret != 0) {
        return TLS_RET_ERR_HOSTNAME;
    }

    // Establecemos el contexto, las funciones de envio y recepcion y el timeout
    mbedtls_ssl_set_bio(&ssl, NULL, (mbedtls_ssl_send_t *) tls_send, (mbedtls_ssl_recv_t *) tls_recv, NULL);

    return TLS_RET_OK;
}

uint16_t TLS_send(char * buffer, uint16_t bytes) {
    return mbedtls_ssl_write(&ssl, (const unsigned char *) buffer, bytes);
}

uint16_t TLS_recv(char * buffer, uint16_t bytes) {
    return mbedtls_ssl_read(&ssl, (unsigned char *) buffer, bytes);
}


