/*
 * esp_client.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad para poder usar el modulo ESP01
 */

#include "esp_client.h"

#include <stdio.h>
#include <string.h>

#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>
#include <ti/devices/msp432p4xx/driverlib/uart.h>

#include "console_client/console_client.h"

/* Definicion de comandos AT */
#define ESP_AT_RST              "AT+RST"
#define ESP_STA_MODE            "AT+CWMODE=1"
#define ESP_LIST_AP             "AT+CWLAP"
#define ESP_CONN_AP             "AT+CWJAP"
#define ESP_TCP                 "AT+CIPSTART=\"TCP\""
#define ESP_TRANSPARENT         "AT+CIPMODE=1"
#define ESP_TCP_SEND            "AT+CIPSEND"
#define ESP_TCP_SEND_END        "+++"
#define ESP_TCP_END             "AT+CIPCLOSE"
#define ESP_TRANSPARENT_END     "AT+CIPMODE=0"
/*----------------------------*/

/* Variables globales */
static uint8_t esp_read_buffer[ESP_BUFFER_SIZE]; // Buffer de lectura
static uint16_t esp_buffer_index = 0; // Indice del buffer (bytes escritos)
static uint16_t esp_read_index = 0; // Indice de lectura (bytes leidos)

static uint8_t initialized = 0;

// Funci�n externa para que ESP maneje sus tiempos de espera. Debe sobreescribirse
extern void esp_delay(int16_t);

// Configuraci�n de la UART
// Valores calculados usando  http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
// para 12 MHz
static eUSCI_UART_ConfigV1 uart_config =
{
     EUSCI_A_UART_CLOCKSOURCE_SMCLK,
     6,
     8,
     32,
     EUSCI_A_UART_NO_PARITY,
     EUSCI_A_UART_LSB_FIRST,
     EUSCI_A_UART_ONE_STOP_BIT,
     EUSCI_A_UART_MODE,
     EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION
};


/**
  * Reinicia el buffer
  */
static void reset_buffer() {
    esp_buffer_index = 0;
    esp_read_index = 0;
    esp_read_buffer[0] = 0;
}

/**
  * Funciona interna para comprobar si la respuesta esta lista para ser leida
  * @param ms_timeout Tiempo maximo de espera en milisegundos
  */
static uint8_t is_response_ready(int16_t ms_timeout) {
    // Para saber si la respuesta esta lista, comprobaremos el indice del buffer sea mayor que cero y se mantenga constante durante 1s
    uint16_t last_index;
    uint8_t cycles_same = 0;
    do {
        last_index = esp_buffer_index;
        esp_delay(ms_timeout > 250 ? 250:ms_timeout);
        ms_timeout -= 250;
        if (last_index == esp_buffer_index) {
            cycles_same++;
        }
    } while(ms_timeout > 0 && cycles_same < 2);

    return cycles_same == 2;
}

/**
  * Funcion interna para comprobar que la respuesta de ESP ha sido OK
  * @return 1 si lo ha sido. 0 si no.
  */
static uint8_t check_response_ok(uint16_t ms_timeout) {
    uint8_t res = 0;

    // Comprobaremos cada 500ms que la respuesta sea ok, hasta que ocurra el timeout o sea 1
    do {
        if(is_response_ready(500)) {
            // Deshabilitamos las interrupciones para que no se sobreescribar el buffer
            MAP_Interrupt_disableMaster();

            // Finalizamos la string para que strstr pare ahi
            esp_read_buffer[esp_buffer_index + 1] = 0;

            // Comprobamos si existe OK en el buffer
            res = strstr((char *) esp_read_buffer, "OK") != NULL;

            // Habilitamos de nuevo las interrupciones
            MAP_Interrupt_enableMaster();
        }
        ms_timeout -= 500;
    } while(ms_timeout > 0 && res == 0);

    // Limpiamos el buffer
    reset_buffer();

    return res;
}

/**
  * Escribe un comando AT
  * @param to_write. Mensaje a escribir
  */
static void ESP_write_command(const char * to_write) {
    uint8_t i;
    uint8_t len = strlen(to_write);
    for (i = 0; i < len; i++) {
        MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) to_write[i]);
    }
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) '\r');
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) '\n');
}

int16_t ESP_read_bytes(unsigned char * buffer, int16_t bytes) {
    int16_t res;

    // Calculamos cuanto es el m�ximo que se puede leer
    uint16_t to_read = (esp_buffer_index - esp_read_index)  < bytes ? (esp_buffer_index - esp_read_index):bytes;

    // Deshabilitamos las interrupciones para que no se sobreescribar el buffer
    MAP_Interrupt_disableMaster();

    // Se lee teniendo en cuenta el �ltimo punto que se ley�.
    int16_t read_padding = esp_read_index;

    for (res = 0; res < to_read; res++) {
        buffer[res] = esp_read_buffer[read_padding + res];

        // Si se ha leido el mensaje completo se limpia el buffer y se deja de leer.
        if (++esp_read_index == esp_buffer_index) {
            res++;
            reset_buffer();
            break;
        }
    }

    // Habilitamos de nuevo las interrupciones
    MAP_Interrupt_enableMaster();
    return res;
}

int16_t ESP_write_bytes(const unsigned char * to_write, uint16_t bytes) {
    uint16_t i;

    // Deshabilitamos las interrupciones para que no se sobreescribar el buffer
    MAP_Interrupt_disableMaster();

    for (i = 0; i < bytes; i++) {
        MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) to_write[i]);
    }

    // Habilitamos de nuevo las interrupciones
    MAP_Interrupt_enableMaster();
    return i;
}


/**
  * Cambia el ESP al modo estacion.
  * @return 1 si la respuesta ha sido ok. 0 si ha fallado.
  */
static uint8_t ESP_set_station_mode() {
    // Reseteamos el buffer
    reset_buffer();

    // Enviamos AT+CWMODE=1 para cambiar el modo a estaci�n
    ESP_write_command(ESP_STA_MODE);

    return check_response_ok(2000);
}


/**
  * Cambia el ESP a modo TCP transparente
  * @return 1 si la respuesta ha sido ok. 0 si ha fallado.
  */
static uint8_t ESP_set_transparent_mode() {
    // Reseteamos el buffer
    reset_buffer();
    ESP_write_command(ESP_TRANSPARENT);

    return check_response_ok(1000);
}


/**
  * Preparar un envio TCP en modo transparente
  * @return 1 si la respuesta ha sido ok. 0 si ha fallado.
  */
static uint8_t ESP_start_sending() {
    ESP_write_command(ESP_TCP_SEND);

    return check_response_ok(1000);
}


/**
  * Finalizar un envio TCP en modo transparente
  */
static void ESP_finish_sending() {
    // Para finalizar un envio en modo transparente se debe enviar +++ y esperar al menos 1 segundo.
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) '+');
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) '+');
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint8_t) '+');
    esp_delay(1000);
}

/**
  * Resetea el dispositivo para eliminar conexiones antiguas.
  * @return 1 si la respuesta ha sido ok. 0 si ha fallado.
  */
static uint8_t ESP_reset() {
    // Reseteamos el buffer
    reset_buffer();

    // Enviamos AT+RST para resetear el dispositivo
    ESP_write_command(ESP_AT_RST);

    return check_response_ok(1000);
}

uint8_t ESP_connect_ap(const char * ssid, const char * password) {
    // El tama�o es el peor caso. 14 caracteres del comando, 1 del final de la string, 32 del SSID y 63 de la contrasenya
    char at_command[110];
    sprintf(at_command, "%s=\"%s\",\"%s\"", ESP_CONN_AP, ssid, password);

    // Reseteamos el buffer
    reset_buffer();

    // Mandamos el comando
    ESP_write_command(at_command);

    return check_response_ok(15000);
}

uint8_t ESP_tcp_connect(const char *ip, const char *port) {
    // El tama�o es el peor caso. 22 caracteres del comando, 1 del final de la string, 17 de la IP y 5 del puerto
    char at_command[45];
    sprintf(at_command, "%s,\"%s\",%s", ESP_TCP, ip, port);

    // Mandamos el comando
    ESP_write_command(at_command);

    // Esperamos
    return check_response_ok(5000) && ESP_set_transparent_mode() && ESP_start_sending();
}

void ESP_tcp_send(const char * to_write) {
    ESP_write_command(to_write);
    esp_delay(1000);
}

int16_t ESP_tcp_send_bytes(const unsigned char * to_write, int16_t bytes) {
    int16_t ret = ESP_write_bytes(to_write, bytes);
    esp_delay(1000);
    return ret;
}

uint8_t ESP_tcp_close() {
    uint8_t res;
    ESP_finish_sending();
    esp_delay(1000);
    ESP_write_command(ESP_TRANSPARENT_END);
    res = check_response_ok(1000);
    ESP_write_command(ESP_TCP_END);
    res &= check_response_ok(1000);
    return res;

}

uint8_t ESP_init() {
    uint8_t res = 0;

    if (initialized == 0) {
        // Pines para UART2 (ESP01)
        MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P3, GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

        // Inicializamos el modulo
        MAP_UART_initModule(EUSCI_A2_BASE, &uart_config);

        // Lo habilitamos
        MAP_UART_enableModule(EUSCI_A2_BASE);

        // Activamos la interrupci�n
        MAP_UART_enableInterrupt(EUSCI_A2_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

        // La habilitamos
        MAP_Interrupt_enableInterrupt(INT_EUSCIA2);

        // Cambiamos la salida al pin de Reset para encender el dispositivo
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN5);

        // Esperamos cierto tiempo para que termine de arrancar
        esp_delay(1000);
    }

    // Guardamos que se he inicializado.
    initialized = 1;

    // Reseteamos el dispositivo, y, al mismo tiempo, se comprueba si funciona
    res = ESP_reset();

    if (res == 1) {
        // El dispositivo funciona, procedemos a cambiar el modo a estaci�n
        res = ESP_set_station_mode();
    }

    return res;
}

/**
  * ISR para la UART2
  */
void EUSCIA2_IRQHandler(void) {
    // Comprobamos si la interrupci�n verdaderamente se ha producido
    uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A2_BASE);
    MAP_UART_clearInterruptFlag(EUSCI_A2_BASE, status);

    if(status & EUSCI_A_UART_RECEIVE_INTERRUPT)
    {
        if (esp_buffer_index < ESP_BUFFER_SIZE) {
            // Recibimos un byte
            esp_read_buffer[esp_buffer_index] = MAP_UART_receiveData(EUSCI_A2_BASE);
        }
        esp_buffer_index++;
    }
}

