/*
 * esp_client.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef ESP_CLIENT_H_
#define ESP_CLIENT_H_

#include <stdint.h>


#define ESP_BUFFER_SIZE     2048


/**
  * Inicializa el modulo ESP en modo estacion.
  * @return 1 si esta listo para funcionar. 0 si ha fallado.
  */
uint8_t ESP_init();

/**
  * Conecta con un punto de acceso.
  * @param ssid. SSID del punto de acceso.
  * @param password. Contrasenya del punto de acceso
  * @return 1 si se ha conectado. 0 si ha fallado.
  */
uint8_t ESP_connect_ap(const char* ssid, const char* password);

/**
  * Comienza una conexion TCP en modo de env�o transparente.
  * @param ip. Direccion IP
  * @param port. Puerto
  * @return 1 si se ha conectado y esta listo para enviar. 0 si ha fallado.
  */
uint8_t ESP_tcp_connect(const char* ip, const char* port);

/**
  * Envia a traves de la conexion TCP transparente activa.
  * @param to_write. Mensaje a enviar.
  */
void ESP_tcp_send(const char * to_write);

/**
  * Envia una cantidad determinada de bytes a traves de la conexion TCP transparente activa.
  * @param to_write. Buffer del que se leeran los bytes.
  * @param bytes. Cantidad de bytes a enviar
  * @return byes enviados.
  */
int16_t ESP_tcp_send_bytes(const unsigned char * to_write, int16_t bytes);

/**
  * Para el envio, cierra la conexion y desactiva el modo transparente.
  * @param bytes. Cantidad de bytes a enviar
  */
uint8_t ESP_tcp_close();

/**
  * Lee una cantidad de bytes del buffer. Si se ha leido por completo se limpia.
  * @param buffer Buffer donde almacenar lo le�do
  * @param bytes Cantidad de bytes a leer
  * @return -1 si el mensaje no est� listo o longitud del mensaje leido.
  */
int16_t ESP_read_bytes(unsigned char * buffer, int16_t bytes);

/**
  * Escribe una cantidad determinada de bytes.
  * @param to_write Mensaje a escribir
  * @param bytes Cantidad de bytes a escribir
  * @return bytes enviados
  */
int16_t ESP_write_bytes(const unsigned char * to_write, uint16_t bytes);


#endif /* ESP_CLIENT_H_ */
