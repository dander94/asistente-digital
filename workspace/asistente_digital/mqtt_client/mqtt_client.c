/*
 * mqtt_client.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad para usar MQTT en este proyecto
 */

#include "mqtt_client.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>

/* POSIX */
#include <pthread.h>

/* RTOS Helpers */
#include "rtos/rtos.h"

/* Eclipse PAHO */
#include "MQTTPacket/MQTTPacket.h"

/* Driver de ESP */
#include "esp_client/esp_client.h"
#include "esp_client/tls.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* ------------------------ */

/* Mutex  para el uso de ESP */
#define MQTT_MUTEX_TIMEOUT_TIME     15 // 15 Segundos
#define MQTT_KEEP_ALIVE             180 // 180 Segundos

static pthread_mutex_t mqtt_buffer_mutex;
static uint8_t initialized = 0;
/* ------------------------- */


/* Buffer de lectura desde ESP */
#define MQTT_BUFFER_AT_POSITION     mqtt_buffer + mqtt_buffer_index
#define MQTT_BUFFER_LEN_LEFT        MQTT_BUFFER_SIZE - mqtt_buffer_index

static unsigned char mqtt_buffer[MQTT_BUFFER_SIZE];
static uint8_t mqtt_buffer_index = 0;
/* ---------------------------- */

/*
 * Funcion para definir las esperas en esp
 */
void esp_delay(int16_t ms) {
    task_delay(ms);
}

/*
 * Adquiere el mutex
 * @return 1 si se ha adquirido. 0 si no.
 */
static uint8_t mutex_lock() {
    struct timespec mutex_timeout;
    clock_gettime(CLOCK_REALTIME, &mutex_timeout);
    mutex_timeout.tv_sec += MQTT_MUTEX_TIMEOUT_TIME;
    return initialized != 0 && pthread_mutex_timedlock(&mqtt_buffer_mutex, &mutex_timeout) == 0;
}

/*
 * Libera el mutex.
 * @return 1 si se ha liberado correctamente. 0 si no.
 */
static uint8_t mutex_unlock() {
    return initialized != 0 && pthread_mutex_unlock(&mqtt_buffer_mutex) == 0;
}

/*
 * Limpia el buffer
 */
static void reset_buffer() {
    mqtt_buffer[0] = 0;
    mqtt_buffer_index = 0;
}

/*
 * Inicializa el modulo MQTT
 * @return 1 si se ha incializado correctamente. 0 si ha fallado.
 */
static uint8_t MQTT_init() {
    uint8_t res;

    // Limpiamos el buffer
    reset_buffer();
    if (initialized == 0) {
        // Creamos el mutex si no se habia creado.
        res = pthread_mutex_init(&mqtt_buffer_mutex, NULL) == 0;
    }
    initialized |= res;
    return res;
}


/*
 * Publica un mensaje en un topic con una determinada QoS.
 * @param topic Topic en el que se publicara el mensaje
 * @param message Mensaje que se publicara
 * @param qos QoS. Puede ser 0 (sin confirmacion) o 1 (con confirmacion)
 * @return 1 si se ha publicado correctamente. 0 si no.
 */
static uint8_t MQTT_publish(char * topic, char * message, uint8_t qos) {
    uint8_t res = 0;
    uint8_t message_len = strlen(message);

    // Se crea una id de mensaje aleatoria.
    unsigned short message_id = rand() % 255;

    // Se crea la MQTTString para el topic
    MQTTString topic_str = MQTTString_initializer;
    topic_str.cstring = topic;

    if(mutex_lock()) {
        // Se serializa el paquete MQTT Publish en el buffer
        mqtt_buffer_index += MQTTSerialize_publish(MQTT_BUFFER_AT_POSITION, MQTT_BUFFER_LEN_LEFT, 0, qos, 0, message_id, topic_str, (unsigned char *) message, message_len);

        // Se envia el buffer
        TLS_send((char *) mqtt_buffer, mqtt_buffer_index);

        task_delay(1500);

        if (qos == 1) {
            // Si QoS era igual a 1 se comprueba si se ha recibido la PUBACK. Para ello se lee desde ESP la respuesta.
            res = MQTTPacket_read(mqtt_buffer, MQTT_BUFFER_SIZE, (int (*)(unsigned char *, int)) TLS_recv) == PUBACK;

            if (res) {
                unsigned short rec_msg_id;

                // Se comprueba que la ID del mensaje del PUBACK es la misma que la id del mensaje que se envio.
                res = MQTTDeserialize_ack((unsigned char *) PUBACK, NULL, &rec_msg_id, (unsigned char *) mqtt_buffer, MQTT_BUFFER_SIZE) && message_id == rec_msg_id;
            }
        } else {
            res = 1;
        }

        reset_buffer();
        mutex_unlock();
    }
    return res;
}

/*
 * Suscribirse a un topic
 * @param topic Topic al que suscribirse
 * @return 1 si se ha suscrito correctamente. 0 si ha fallado.
 */
static uint8_t MQTT_subscribe(char * topic) {
    uint8_t res = 0;
    uint8_t qos = 0;

    // Se crea la string para el topic.
    MQTTString topic_str = MQTTString_initializer;
    topic_str.cstring = topic;

    if (mutex_lock()) {
        // Se serializa el subscribe, con qos 0 indicando que no se devolveran ACK
        mqtt_buffer_index += MQTTSerialize_subscribe(mqtt_buffer, MQTT_BUFFER_SIZE, 0, 1, 1, &topic_str, (int *) &qos);

        TLS_send((char *) mqtt_buffer, mqtt_buffer_index);

        task_delay(2000);

        // Se comprueba si se ha recibido el SUBACK
        res = MQTTPacket_read(mqtt_buffer, MQTT_BUFFER_SIZE, (int (*)(unsigned char *, int)) TLS_recv) == SUBACK;
        if (res) {
            int8_t granted_qos;

            // Se deserializa el SUBACK para comprobar si el QoS garantizado no es 128, lo cual seria un error.
            MQTTDeserialize_suback(0, 1, 0, (int *) &granted_qos, mqtt_buffer, MQTT_BUFFER_SIZE);

            res = granted_qos != 128;
        }
        goto ret_unlock;
    } else {
        return res;
    }
ret_unlock:
    reset_buffer();
    mutex_unlock();
    return res;
}

uint8_t MQTT_ping_loop(uint8_t * stop_flag) {
    uint8_t failed_ping = 0;

    // Bucle hasta que se indique que pare con la stop flag o hasta que falle un ping
    while (*stop_flag != 1 || failed_ping == 1) {
        if (mutex_lock()) {
            reset_buffer();

            // Serializamos el paquete MQTT Ping
            mqtt_buffer_index += MQTTSerialize_pingreq(mqtt_buffer, MQTT_BUFFER_SIZE);

            TLS_send((char *) mqtt_buffer, mqtt_buffer_index);

            task_delay(2000);

            // Se comprueba que la respuesta ha sido un PINGRESP
            failed_ping = MQTTPacket_read(mqtt_buffer, MQTT_BUFFER_SIZE, (int (*)(unsigned char *, int)) TLS_recv) != PINGRESP;

            reset_buffer();
            mutex_unlock();
        }

        if (!failed_ping) {
            // Si el ping no ha fallado, se suspende la tarea hasta el siguiente ping
            task_delay(MQTT_KEEP_ALIVE * 0.5);
        }
    }
    return failed_ping == 0;
}

uint8_t MQTT_connect(char * ssid, char *password) {
    unsigned char ack;

    // Inicializamos ESP
    if(!ESP_init()) {
        return MQTT_RET_ERR_ESP;
    }

    // Conectamos con el AP
    if(!ESP_connect_ap(ssid, password)) {
        return MQTT_RET_ERR_AP;
    }

    // Iniciamos TLS
    if(TLS_init() != TLS_RET_OK) {
        return MQTT_RET_ERR_TLS;
    }

    // Inicializamos el paquete MQTT para conectar
    MQTTPacket_connectData connect = MQTTPacket_connectData_initializer;

    // Inicializamos el modulo MQTT
    uint8_t res = MQTT_init();

    if (res != 1) {
        return MQTT_RET_ERR_INIT;
    }

    // Definimos los parametros de la conexion.
    connect.clientID.cstring = ASISTENTE_DEVICE_ID;
    connect.keepAliveInterval = MQTT_KEEP_ALIVE;
    connect.cleansession = 0;
    connect.username.cstring = BROKER_USERNAME;
    connect.password.cstring = BROKER_PASS;

    if(mutex_lock()) {
        // Serializamos en el buffer el paquete CONNECT se va a enviar.
        mqtt_buffer_index += MQTTSerialize_connect(mqtt_buffer, MQTT_BUFFER_LEN_LEFT, &connect);

        // Se establece la conexion TCP en modo transparente con el broker.
        if(ESP_tcp_connect(BROKER_IP, BROKER_PORT)) {
            // Se envia el paquete
            TLS_send((char *) mqtt_buffer, mqtt_buffer_index);

            // Se espera a la respuesta
            task_delay(1000);

            // Se limpia el buffer
            reset_buffer();

            // Se lee la respuesta desde ESP y se comprueba si es un paquete CONNACK
            if (MQTTPacket_read(mqtt_buffer, MQTT_BUFFER_SIZE, (int (*)(unsigned char *, int)) TLS_recv) == CONNACK) {
                // Se deserializa para recupera la ack y se comprueba que esta sea cero, es decir, que ha ido bien.
                res = MQTTDeserialize_connack(NULL, &ack, mqtt_buffer, MQTT_BUFFER_SIZE) != 1 || ack != 0;
                if (res != 0) {
                    res = MQTT_RET_ERR_CONNACK;
                } else {
                    res = MQTT_RET_CONNECT_OK;
                }
            }
        } else {
            res = MQTT_RET_ERR_TCP;
        }

        // Se limpia el buffer
        reset_buffer();

        // Se libera el mutex
        mutex_unlock();
    }
    return res;
}

uint8_t MQTT_disconnect() {
    if(mutex_lock()) {
        reset_buffer();

        // Se serializa el paquete MQTT Disconnect
        mqtt_buffer_index += MQTTSerialize_disconnect(mqtt_buffer, MQTT_BUFFER_SIZE);

        // Se envia el paquete disconnect
        TLS_send((char *) mqtt_buffer, mqtt_buffer_index);

        task_delay(2000);

        reset_buffer();
        mutex_unlock();
    }

    // Se cierra la conexion TCP
    return ESP_tcp_close();
}

uint8_t MQTT_subscribe_all() {
    // Variable para el topic al que se suscribira. Id del dispositivo (8 longitud) + /# (2 longitud) + 1 final string
    char subscription_topic[11] = ASISTENTE_DEVICE_ID;

    // Concatenamos el apendice para el topic
    strcat(subscription_topic, "/#");

    return MQTT_subscribe(subscription_topic);
}

void MQTT_subscription_loop(void on_message(char*, char *), uint8_t * stop_flag) {
    uint8_t qos = 0;

    while (*stop_flag != 1) {
        if (mutex_lock()) {
            // Se lee el buffer de ESP y se comprueba si hay un mensaje y es de tipo PUBLISH
            if (MQTTPacket_read(mqtt_buffer, MQTT_BUFFER_SIZE, (int (*)(unsigned char *, int)) TLS_recv) == PUBLISH) {
                uint8_t payload_len;
                MQTTString topic;
                char* payload;

                // Se recupera el topic y el mensaje, asi como sus longitudes
                MQTTDeserialize_publish(0, (int *) &qos, 0, 0, &topic,
                        (unsigned char **) &payload, (int *) &payload_len, mqtt_buffer, MQTT_BUFFER_SIZE);

                // Se crean nuevas string de las longitudes recibidas con el topic y el mensaje.
                char * text_topic = malloc(topic.lenstring.len);
                char * text_payload = malloc(payload_len);

                memcpy(text_topic, topic.lenstring.data, topic.lenstring.len);
                memcpy(text_payload, payload, payload_len);

                // Se llama a la funcion de callback con el topic y el mensaje
                on_message(text_topic, text_payload);
                reset_buffer();
            }
            mutex_unlock();
        }
        task_delay(500);
    }
}

uint8_t MQTT_ask_sync_code() {

    // Topic para enviar el mensaje. 4 char del identificador, 1 barra, 8 device id + 1 fin string
    char topic_str[14];

    sprintf(topic_str, "%s/%s", MQTT_TOPIC_SYNC, ASISTENTE_DEVICE_ID);

    return MQTT_publish(topic_str, "", 0);
}

uint8_t MQTT_publish_notification(char * notification) {
    // Topic para enviar el mensaje. 4 char del identificador, 1 barra, 8 device id + 1 fin string
    char topic_str[14];

    sprintf(topic_str, "%s/%s", MQTT_TOPIC_NOTIF, ASISTENTE_DEVICE_ID);

    return MQTT_publish(topic_str, notification, 1);
}

uint8_t MQTT_ask_conf() {

    // Topic para enviar el mensaje. 4 char del identificador, 1 barra, 8 device id + 1 fin string
    char topic_str[14];

    sprintf(topic_str, "%s/%s", MQTT_TOPIC_CONF, ASISTENTE_DEVICE_ID);

    return MQTT_publish(topic_str, "", 0);
}

uint8_t MQTT_publish_heartb() {
    // Topic para enviar el mensaje. 4 char del identificador, 1 barra, 8 device id + 1 fin string
    char topic_str[14];
    sprintf(topic_str, "%s/%s", MQTT_TOPIC_HEARTB, ASISTENTE_DEVICE_ID);

    return MQTT_publish(topic_str, "", 0);
}
