/*
 * ambiente.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef AMBIENTE_AMBIENTE_H_
#define AMBIENTE_AMBIENTE_H_

#include <stdint.h>


/*
 * Inicializa los sensores de temperatura y luz
 */
void SENSORS_init();

/*
 * Comprueba si la temperatura es superior a la minima
 * @return 1 si es superior o no est� configurada. 0 si es inferior
 */
uint8_t SENSORS_check_min_temperature();

/*
 * Comprueba si la temperatura es superior a la maxima
 * @return 1 si es inferior o no est� configurada. 0 si es superior
 */
uint8_t SENSORS_check_max_temperature();

/*
 * Comprueba si la luz esta encendida con el noche activado
 * @return 1 si no hay luz encendida. 0 si la hay
 */
uint8_t SENSORS_check_light();

/*
 * Guarda el valor del modo noche
 * @param night_mode_enabled. 1 para indicar que esta habilitado. 0 para indicar que no lo esta.
 */
void SENSORS_set_night_mode(uint8_t night_mode_enabled);

/*
 * Guarda el valor de la temperatura maxima
 * @param new_max_temperature. Valor de la temperatura maxima
 */
void SENSORS_set_max_temperature(uint8_t new_max_temperature);

/*
 * Guarda el valor de la temperatura minima
 * @param new_min_temperature. Valor de la temperatura minima
 */
void SENSORS_set_min_temperature(uint8_t new_min_temperature);

#endif /* AMBIENTE_AMBIENTE_H_ */
