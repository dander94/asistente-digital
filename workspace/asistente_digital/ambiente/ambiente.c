/*
 * ambiente.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero incluye la funcionalidad para trabajar con los sensores
 */

#include "ambiente.h"

#include "drivers/HAL_I2C.h"
#include "drivers/HAL_OPT3001.h"
#include "drivers/HAL_TMP006.h"

#include "configuracion/configuracion.h"


/* Variables globales para el modo noche y la temperatura */
uint8_t night_mode = 0;
uint8_t max_temperature = 0;
uint8_t min_temperature = 0;

/*
 * Convierte la temperatura en entero y en grados Celsius
 * @return temperatura en celsius
 */
static uint8_t get_celsius_temperature() {
    return (uint8_t) ((TMP006_getTemp() - 32) * 5 / 9);
}


void SENSORS_init() {
    // Inicia I2C
    Init_I2C_GPIO();
    I2C_init();

    // Inicia el sensor de Temperatura
    TMP006_init();

    // Inicia el sensor de Luz
    OPT3001_init();
}


uint8_t SENSORS_check_min_temperature() {
    return min_temperature == 0 || get_celsius_temperature() > min_temperature;
}


uint8_t SENSORS_check_max_temperature() {
    return max_temperature == 0 || get_celsius_temperature() < max_temperature;
}


uint8_t SENSORS_check_light() {
    return night_mode == 0 || OPT3001_getLux() < LIGHT_SENSIBIILITY;
}

void SENSORS_set_night_mode(uint8_t night_mode_enabled) {
    night_mode = night_mode_enabled;
}

void SENSORS_set_max_temperature(uint8_t new_max_temperature) {
    max_temperature = new_max_temperature;
}

void SENSORS_set_min_temperature(uint8_t new_min_temperature) {
    min_temperature = new_min_temperature;
}

