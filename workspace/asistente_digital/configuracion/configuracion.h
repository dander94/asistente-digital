/*
 * configuracion.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la configuracion global del dipositivo
 */

#ifndef CONFIGURACION_CONFIGURACION_H_
#define CONFIGURACION_CONFIGURACION_H_

#include <stdint.h>

/* Dispositivo */
#define ASISTENTE_DEVICE_ID           "12c7e82X"
#define DEVICE_ID_LEN                 8
#define DEVICE_VOLUME                 1

/* Configuraci�n del Wifi */
#define WIFI_SSID           "asistente_digital"
#define WIFI_PASS           "z4qqqbtm"

#define SSID_MAX_LEN                32
#define PASS_MAX_LEN                63

/* Configuracion del Broker y MQTT */
#define BROKER_IP           "167.71.41.147"
#define BROKER_PORT         "8883"
#define BROKER_USERNAME     "msp_notifier"
#define BROKER_PASS         "4]6<u,yMQ{fa^rht}lsJuS+B<[2U0N"

#define MQTT_TOPIC_SYNC     "sync" // Sync
#define MQTT_TOPIC_CONF     "conf" // Configuracion
#define MQTT_TOPIC_NOTIF    "notf" // Notificacion
#define MQTT_TOPIC_HEARTB   "hrtb" // Heartbeat

#define SYNC_CODE_LEN       9
#define TOPIC_LEN           DEVICE_ID_LEN + 4
#define SENSORS_CONF_LEN    7

#define HEARTB_SECONDS                  15
#define HEARTB_RESP_MAX_SECONDS         35
/* SENSORS */
#define TEMP_SENSOR_CHECK_MINUTES       5 // Se comprobara cada 5 minutos como maximo
#define LIGHT_SENSOR_CHECK_MINUTES      1 // Se comprobara cada 1 minuto como maximo
#define LIGHT_SENSIBIILITY              20 // A partir de este valor se considerara que hay una luz encendida

/* Identificador de notificaciones*/
#define NOTIFICATION_TYPE_UP            "up"
#define NOTIFICATION_TYPE_LEFT          "le"
#define NOTIFICATION_TYPE_DOWN          "do"
#define NOTIFICATION_TYPE_RIGHT         "ri"
#define NOTIFICATION_TYPE_UP_LEFT       "ul"
#define NOTIFICATION_TYPE_UP_RIGHT      "ur"
#define NOTIFICATION_TYPE_TEMP_HIGH     "th"
#define NOTIFICATION_TYPE_TEMP_LOW      "tl"
#define NOTIFICATION_TYPE_LIGHT         "lo"

/* Estado del sistema */
typedef enum {
    _SYS_OFFLINE, _SYS_CONNECTED, _SYS_READY, _SYS_ERROR
} SystemStatus;


/* Funciones */

/*
 * Inicializa el modulo de configuracion
 * @return 1 si se ha iniciado correctamente. 0 si no.
 */
uint8_t CONFIG_init();

/*
 * Cambia el estado del sistema
 * @param new_system_status Nuevo estado del sistema
 * @return 1 si lo ha cambiado. 0 si no.
 */
uint8_t CONFIG_set_system_status(SystemStatus new_system_status);

/*
 * Recupera el estado del sistema
 * @return Estado actual del sistema
 */
SystemStatus CONFIG_get_system_status();

/*
 * Metodo para leer las crdenciales Wi-Fi que se deben usar.
 * Leera de la memoria y, si no hay nada, devolvera las credenciales por defecto.
 * @param ssid. Puntero donde almacenar el ssid.
 * @param password. Puntero donde almacenar la contrasenya.
 */
void CONFIG_read_wifi(char * ssid, char * password);

/*
 * Funcion para almacenar las credenciales Wi-Fi
 * @param ssid. Cadena con el ssid
 * @param ssid_len. Longitud del ssid
 * @param password. Cadena con la contrasenya.
 * @param pass_len. Longitud de la contrasenya
 * @return 1 si se ha almacenado correctamente. 0 si no.
 */
uint8_t CONFIG_save_wifi(char * ssid, uint8_t ssid_len,  char * password, uint8_t pass_len);

/*
 * Habilita el boton de sync
 */
void CONFIG_enable_sync_button();
#endif /* CONFIGURACION_CONFIGURACION_H_ */
