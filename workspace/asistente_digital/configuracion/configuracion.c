/*
 * configuracion.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene funciones para trabajar con la configuracion del dispositivo y evitar asi variables globales
 */

#include "configuracion.h"

#include <string.h>

/* MSP 432 SDK */
#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <ti/devices/msp432p4xx/driverlib/timer_a.h>
#include <ti/devices/msp432p4xx/driverlib/flash.h>
#include <ti/devices/msp432p4xx/driverlib/reset.h>

/* POSIX */
#include <pthread.h>

/* Console Client */
#include "console_client/console_client.h"

/* RTOS */
#include "rtos/rtos.h"

/* Escribiremos en el Banco 1, Sector 31. Sector 63 si se cuentan los del banco 0.
*   La eleccion de este sector es porque datos del programa se almacenan en la MAIN MEMORY, por lo que
*   La ultima seccion de esta memoria es la que probablmente este vacia a medida que el programa crezca.
*/
#define MEMORY_BANK             FLASH_MAIN_MEMORY_SPACE_BANK1
#define MEMORY_SECTOR           FLASH_SECTOR31
#define WIFI_START_MEMORY       0x0003F000
#define MEMORY_PASS_START       MEMORY_SSID_START + SSID_LEN

// Escribiremos el sector completo.
/* Escribiremos en la memoria de la siguiente manera. Supongase cada linea como 16 bytes
 1 INIT
 2 SSID (1)
 3 SSID (2)
 4 SSID(3) \0 PASSWORD (1)
 5 PASSWORD (2)
 6 PASSWORD (3)
 7 PASSWORD (4)
 8 \0
*/
#define SSID_LEN                SSID_MAX_LEN + 1  // Espacio para el fin de string
#define PASS_LEN                PASS_MAX_LEN + 1
#define INIT_LEN                16

#define SECTOR_PORTION_SIZE     4096
#define SECTOR_SSID_OFFSET      INIT_LEN
#define SECTOR_PASS_OFFSET      SECTOR_SSID_OFFSET + SSID_LEN

/* Variables Sync Code & Reset */
static uint8_t sync_code_checks = 0;
static uint8_t reset_checks = 0;


/* Variables de configuracion */
SystemStatus system_status = _SYS_OFFLINE;

/* Sys Status Mutex */
static pthread_mutex_t system_status_mutex;

/* Contenedor auxiliar para escribir en la memoria */
static char sector[SECTOR_PORTION_SIZE] = "INIT";


/* Configuracion del Timer de los botones*/
static const Timer_A_UpModeConfig timer_config =
{
         TIMER_A_CLOCKSOURCE_ACLK, // 32 Khz
         TIMER_A_CLOCKSOURCE_DIVIDER_32,
         1024,                // 32768 / 32
         TIMER_A_TAIE_INTERRUPT_DISABLE,
         TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,
         TIMER_A_DO_CLEAR
};


/*
 * Funcion para borrar la memoria y reiniciar el dispositivo.
 */
static void factory_reset() {
    // Desprotegemos el sector
    MAP_FlashCtl_unprotectSector(MEMORY_BANK, MEMORY_SECTOR);

    // Borramos todos los sectores desprotegidos. En este caso sera el sector que hemos desprotegido previamente.
    MAP_FlashCtl_performMassErase();

    // Protegemos el sector de nuevo
    MAP_FlashCtl_protectSector(MEMORY_BANK, MEMORY_SECTOR);

    // Reiniciamos el dispositivo
    ResetCtl_initiateHardReset();
}

/*
 * Funcion para escribir la variable sector en la memoria.
 * @return 1 si se ha escrito correctamente. 0 si no.
 */
static uint8_t write_sector() {
    uint8_t res;

    // Desprotegemos el sector
    MAP_FlashCtl_unprotectSector(MEMORY_BANK, MEMORY_SECTOR);

    // Borramos todos los sectores desprotegidos. En este caso sera el sector que hemos desprotegido previamente.
    if(!MAP_FlashCtl_performMassErase()) {
        res = 0;
        goto exit_and_return;
    }

    // Escribimos el sector
    if(!MAP_FlashCtl_programMemory (sector, (void*) WIFI_START_MEMORY, SECTOR_PORTION_SIZE)) {
        res = 0;
        goto exit_and_return;
    }

    res = 1;
    goto exit_and_return;

exit_and_return:
     // Protegemos el sector de nuevo
     MAP_FlashCtl_protectSector(MEMORY_BANK, MEMORY_SECTOR);
     return res;
}

/*
 * Comprueba e incializa la memoria.
 * @return 1 si se ha iniciado correctamente o estaba iniciada. 0 si no ha podido iniciar la memoria.
 */
static uint8_t initialize_memory() {
    char initialized[16];

    // Copiamos los 16 primeros bytes de la memoria. Debe ser una cadena igual a INIT, para indicar que se ha inciializado al menos una vez. En caso contrario, se inicializara
    // llena de ceros, para evitar cadenas erroneas y poder trabajar con el ssid y la pass.
    memcpy(initialized, (char *) WIFI_START_MEMORY, 16);

    if (strcmp(initialized, "INIT") != 0) {
        // Escribimos el sector vacio en inicializado
        return write_sector();
    }
    return 1;
}

/*
 * Habilita el boton de factory reset
 */
static void enable_reset_button() {
    // Reset del flag de interrupcion del pin 1.4 (Boton S2 de la placa MSP432)
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P1, GPIO_PIN4);

    // Configuracion del pin 1.4 (Boton S2 de la placa MSP432) como entrada con R de pull-up
    MAP_GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN4);

    // Habilita la interrupcion del pin 1.4 (Boton S2 de la placa MSP432)
    MAP_GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN4);

    // Establecemos la prioridad de la interrupcion del temporizador para permitir que pueda usar el semaforo
    MAP_Interrupt_setPriority(INT_PORT1, (1 << 5));

    // Habilita la interrupcion del PORT1
    MAP_Interrupt_enableInterrupt(INT_PORT1);

    // Inicializamos el temporizador. Utilizaremos el A2
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);
    MAP_Timer_A_enableCaptureCompareInterrupt(TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);

    // Modo Up
    MAP_Timer_A_configureUpMode(TIMER_A2_BASE, &timer_config);
    MAP_Timer_A_clearInterruptFlag(TIMER_A2_BASE);

    // Habilitamos la interrupcion
    MAP_Interrupt_enableInterrupt(INT_TA2_0);

    reset_checks = 0;
}

uint8_t CONFIG_init() {
    uint8_t res = 0;
    res = pthread_mutex_init(&system_status_mutex, NULL) == 0;
    initialize_memory();
    enable_reset_button();
    return res;
}

void CONFIG_enable_sync_button() {
    // Inicialiazamos el semaforo de sync code
    sem_init(&sync_code_semaphore, 0, 0);

    // Configuracion del pin 1.1 (Boton S1 del MSP432) como entrada con Resistencia de pull-up
    MAP_GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN1);

    // Reset del flag de interrupcion del pin P1.1 (Boton S1 del MSP432)
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P1, GPIO_PIN1);

    // Habilita la interrupcion del pin P1.1 (Boton S1 del MSP432)
    MAP_GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN1);

    sync_code_checks = 0;
}

uint8_t CONFIG_set_system_status(SystemStatus new_system_status) {
    uint8_t res = 0;

    // Adquirimos el mutex
    if (pthread_mutex_lock(&system_status_mutex) == 0) {
        // Modificamos el valor
        system_status = new_system_status;
        pthread_mutex_unlock(&system_status_mutex);
        res = 1;
    }
    return res;
}

SystemStatus CONFIG_get_system_status() {
    SystemStatus res;

    // Adquirimos el mutex
    if (pthread_mutex_lock(&system_status_mutex) == 0) {
        // Recuperamos el valor
        res = system_status;
        pthread_mutex_unlock(&system_status_mutex);
    }
    return res;
}

void CONFIG_read_wifi(char * ssid, char * password) {
    // Variables para leer el ssid de la memoria
    char mem_ssid[SSID_LEN] = {'\0'};
    char mem_pass[PASS_LEN] = {'\0'};

    // Almacenamos los valores de la memoria
    memcpy(mem_ssid, (char *) WIFI_START_MEMORY + SECTOR_SSID_OFFSET, SSID_LEN);
    memcpy(mem_pass, (char *) WIFI_START_MEMORY + SECTOR_PASS_OFFSET, PASS_LEN);

    // Comprobamos si existen. En caso afirmativo los copiamos en los parametros.
    // Si no, se copian los valores por defecto en los par�metros.
    if (strlen(mem_ssid) > 0 && strlen(mem_pass) > 0) {
        strcpy(ssid, mem_ssid);
        strcpy(password, mem_pass);
    } else {
        strcpy(ssid, WIFI_SSID);
        strcpy(password, WIFI_PASS);
    }
}

uint8_t CONFIG_save_wifi(char * ssid, uint8_t ssid_len,  char * password, uint8_t pass_len) {
    uint8_t ii;

    memcpy(sector + SECTOR_SSID_OFFSET, ssid, ssid_len);
    for (ii=ssid_len; ii < SSID_LEN; ii++) {
        sector[SECTOR_SSID_OFFSET + ii] = '\0';
    }

    memcpy(sector + SECTOR_PASS_OFFSET, password, pass_len);
    for (ii=pass_len; ii < PASS_LEN; ii++) {
        sector[SECTOR_PASS_OFFSET + ii] = '\0';
    }
    return write_sector();
}

/* ISR Para el PORT 1 */
// Esta ISR se utiliza para comprobar si se esta realizando un factory reset
void PORT1_IRQHandler(void) {
    uint32_t status;

    // Lee el estado de la interrupcion generada por GPIO_PORT_P1
    status = MAP_GPIO_getEnabledInterruptStatus(GPIO_PORT_P1);

    // Reset del flag de interrupcion del pin que la genera
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P1, status);

    // Comprueba si la interrupcion la genero el pin P1.4
    if((status & GPIO_PIN4) &&  MAP_GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN4) == 0) {
        // Activamos el timer para comprobar que se pulsa las veces correctas en un marco breve de tiempo
        MAP_Timer_A_stopTimer(TIMER_A2_BASE);
        MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);

        if(++reset_checks == 3) {
            reset_checks = 0;
            factory_reset();
        } else {
            MAP_Timer_A_startCounter(TIMER_A2_BASE, TIMER_A_UP_MODE);
        }
        // Comprueba si la interrupcion la genero el pin P1.1
    } else if((status & GPIO_PIN1) && MAP_GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN1) == 0) {
        // Activamos el timer para comprobar que se pulsa las veces correctas en un marco breve de tiempo
        MAP_Timer_A_stopTimer(TIMER_A2_BASE);
        MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);

        if(++sync_code_checks == 3) {
            sync_code_checks = 0;
            sem_post(&sync_code_semaphore);
        } else {
            MAP_Timer_A_startCounter(TIMER_A2_BASE, TIMER_A_UP_MODE);
        }

    }
}


/* ISR Para el Timer A2
 * Se comprobara si se esta llevando a cabo un factory reset o un sync code.
 * */

void TA2_0_IRQHandler (void) {
    // Limpia el flag de interrupcion del Timer_A2
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A2_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);

    reset_checks = 0;
    sync_code_checks = 0;
    MAP_Timer_A_stopTimer(TIMER_A2_BASE);
}
