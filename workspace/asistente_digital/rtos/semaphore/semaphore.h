/*
 * semaphore.c
 *
 *  Created on: 7 dic. 2020
 *      Author: Devel
 */


#include <FreeRTOS.h>
#include <semaphore.h>


/*
 * Semaforos
 */

// Semaforo para cuando se pide un sync code
sem_t sync_code_semaphore;
