/*
 * colas.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef COLAS_COLAS_H_
#define COLAS_COLAS_H_

#include <stdint.h>

#define PUB_Q_NAME          "publish"
#define PUB_Q_MSG_COUNT     5
#define PUB_Q_MSG_SIZE      4 //  (1 tipo + 2 notificacion [es el mas grande] + 1)

/*
 * Metodo para inicializar las colas
 */
void init_queues();

/*
 * Metodo para anyadir un mensaje a la cola de publicacion
 * @param msg_type tipo de mensaje a enviar
 * @param message mensaje a enviar
 * @param menssage_len longitud del mensaje
 * @param timeout tiempo maximo de espera
 * @return 1 si se ha enviado. 0 si timeout.
 */
uint8_t add_publish_queue_message(int8_t msg_type, const char * message, uint8_t message_len, uint16_t timeout);

/*
 * Metodo para leer de la cola de publicacion
 * @param buffer Buffer en el que almacenar el mensaje
 * @param timeout tiempo maximo de espera
 * @return Tamanyo del mensaje o 0 si timeout
 */
uint8_t read_publish_queue(char * buffer, uint16_t timeout);

/*
 * Metodo para anyadir un mensaje a la cola de sync
 * @param sync_code mensaje a enviar
 * @param timeout tiempo maximo de espera
 * @return 1 si se ha enviado. 0 si timeout.
 */
uint8_t add_sync_code_queue(const char * sync_code, uint16_t timeout);

/*
 * Metodo para leer de la cola de sync un sync code
 * @param buffer Buffer en el que almacenar el mensaje
 * @param timeout tiempo maximo de espera
 * @return Tamanyo del mensaje o 0 si timeout
 */
uint8_t read_sync_code_queue(char * buffer, uint16_t timeout);

/*
 * Metodo para anyadir un mensaje a la cola de sensores
 * @param conf mensaje a enviar
 * @param timeout tiempo maximo de espera
 * @return 1 si se ha enviado. 0 si timeout.
 */
uint8_t add_sensors_queue_message(const char * conf, uint16_t timeout);

/*
 * Metodo para leer de la cola de sensores
 * @param buffer Buffer en el que almacenar el mensaje
 * @param timeout tiempo maximo de espera
 * @return Tamanyo del mensaje o 0 si timeout
 */
uint8_t read_sensors_queue(char * buffer, uint16_t timeout);

/*
 * Metodo para anyadir un mensaje a la cola de heartb
 * @param conf mensaje a enviar
 * @param timeout tiempo maximo de espera
 * @return 1 si se ha enviado. 0 si timeout.
 */
uint8_t add_heartb_queue_message(const char * conf, uint16_t timeout);

/*
 * Metodo para leer de la cola de heartb
 * @param buffer Buffer en el que almacenar el mensaje
 * @param timeout tiempo maximo de espera
 * @return Tamanyo del mensaje o 0 si timeout
 */
uint8_t read_heartb_queue(char * buffer, uint16_t timeout);

#endif /* COLAS_COLAS_H_ */
