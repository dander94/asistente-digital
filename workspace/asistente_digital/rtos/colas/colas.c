/*
 * colas.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero incluye la funcionalidad para trabajar con colas de POSIX
 */

#include "colas.h"

#include <stdio.h>
#include <time.h>

/* POSIX */
#include "mqueue.h"

/* RTOS */
#include "../delay/delay.h"

#include "configuracion/configuracion.h"

#define SYNC_Q_NAME         "sync"
#define SYNC_Q_MSG_COUNT    1
#define SYNC_Q_MSG_SIZE     10 //  (9 sync code + 1)

#define SENSORS_Q_NAME      "conf"
#define SENSORS_Q_MSG_COUNT  1
#define SENSORS_Q_MSG_SIZE   SENSORS_CONF_LEN + 1

#define HEARTB_Q_NAME       "heartb"
#define HEARTB_Q_MSG_COUNT  1
#define HEARTB_Q_MSG_SIZE   1

/* Colas */
// Attrs para la queue de publish.
// O_NONBLOCK -> No bloqueante si esta llena
// PUB_Q_MSG_COUNT -> Max numero mensajes
// PUB_Q_MSG_SIZE -> Tamanyo max mensajes
// 0 -> Mensajes en la cola actualmente
static struct mq_attr publish_queue_attr = {O_NONBLOCK, PUB_Q_MSG_COUNT, PUB_Q_MSG_SIZE, 0};

// Attrs para la queue de sync.
// O_NONBLOCK -> No bloqueante si esta llena
// SYNC_Q_MSG_COUNT -> Max numero mensajes
// SYNC_Q_MSG_SIZE -> Tamanyo max mensajes
// 0 -> Mensajes en la cola actualmente
static struct mq_attr sync_queue_attr = {O_NONBLOCK, SYNC_Q_MSG_COUNT, SYNC_Q_MSG_SIZE, 0};

// Attrs para la queue de sensors.
// O_NONBLOCK -> No bloqueante si esta llena
// SENSORS_Q_MSG_COUNT -> Max numero mensajes
// SENSORS_Q_MSG_SIZE -> Tamanyo max mensajes
// 0 -> Mensajes en la cola actualmente
static struct mq_attr sensors_queue_attr = {O_NONBLOCK, SENSORS_Q_MSG_COUNT, SENSORS_Q_MSG_SIZE, 0};

// Attrs para la queue de heartb.
// O_NONBLOCK -> No bloqueante si esta llena
// HEARTB_Q_MSG_COUNT -> Max numero mensajes
// HEARTB_Q_MSG_SIZE -> Tamanyo max mensajes
// 0 -> Mensajes en la cola actualmente
static struct mq_attr heartb_queue_attr = {O_NONBLOCK, HEARTB_Q_MSG_COUNT, HEARTB_Q_MSG_SIZE, 0};

/* Colas */
static mqd_t publish_queue;
static mqd_t sync_queue;
static mqd_t sensors_queue;
static mqd_t heartb_queue;

const struct timespec no_wait = {0, 0};

/*
 * Metodo para anyadir un mensaje a una cola suspendiendo la tarea en lugar de bloqueando el hilo
 * @param queue cola de la que se debe leer
 * @param message mensaje a enviar
 * @param message_len longitud del mensaje a enviar
 * @param write_timeout tiempo maximo de espera
 * @return 1 si se ha enviado. 0 si timeout.
 */
static uint8_t add_timed_message(mqd_t queue, const char * message, uint8_t message_len, uint16_t write_timeout) {
    int8_t res = -1;

    // Intentamos enviarlo dentro del tiempo
    do {
       res = mq_timedsend(queue, message, message_len, 0, &no_wait);
       if (res == -1 && write_timeout > 0) {
           task_delay(write_timeout < 200 ? write_timeout:200);
           write_timeout -= write_timeout < 200 ? write_timeout:200;
       }
    } while(write_timeout > 0 && res == -1);
    return res != 1;
}

/*
 * Metodo para leer de una cola suspendiendo la tarea en lugar de bloqueando el hilo.
 * @param queue cola de la que se debe leer
 * @param buffer buffer en el que se almacena el mensaje
 * @param msg_size tamanyo del mensaje
 * @param read_timeout tiempo maximo de espera
 * @return Tamanyo del mensaje o 0 si timeout
 */
static uint8_t read_from_queue(mqd_t queue, char * buffer, uint8_t msg_size, int16_t read_timeout) {
    int8_t res = -1;

    do {
       res = mq_timedreceive(queue, buffer, msg_size, 0, &no_wait);
       if (res == -1 && read_timeout > 0) {
           task_delay(read_timeout < 200 ? read_timeout:200);
           read_timeout -= read_timeout < 200 ? read_timeout:200;
       }
    } while(read_timeout > 0 && res == -1);
    return res != -1 ? res:0;
}

void init_queues() {
    // Inicializamos las colas
    // Con O_CREAT indicamos que la creamos y con O_RDWR que se va a leer y escribir.
    // Con ( mode_t) 0 indicamos los permisos
    publish_queue = mq_open(PUB_Q_NAME, O_CREAT | O_RDWR, ( mode_t ) 0, &publish_queue_attr);
    sync_queue = mq_open(SYNC_Q_NAME, O_CREAT | O_RDWR, ( mode_t ) 0, &sync_queue_attr);
    sensors_queue = mq_open(SENSORS_Q_NAME, O_CREAT | O_RDWR, ( mode_t ) 0, &sensors_queue_attr);
    heartb_queue = mq_open(HEARTB_Q_NAME, O_CREAT | O_RDWR, ( mode_t ) 0, &heartb_queue_attr);
}

uint8_t add_publish_queue_message(int8_t msg_type, const char * message, uint8_t message_len, uint16_t timeout) {
    char to_publish[2 + message_len];

    // Preparamos el mensaje
    sprintf(to_publish, "%c%s", (char) msg_type, message);

    return add_timed_message(publish_queue, to_publish, 2 + message_len, timeout);
}

uint8_t read_publish_queue(char * buffer, uint16_t timeout) {
    return read_from_queue (publish_queue, buffer, PUB_Q_MSG_SIZE, timeout);
}

uint8_t add_sync_code_queue(const char * sync_code, uint16_t timeout) {
    return add_timed_message(sync_queue, sync_code, SYNC_CODE_LEN, timeout);
}

uint8_t read_sync_code_queue(char * buffer, uint16_t timeout) {
    uint8_t res = read_from_queue (sync_queue, buffer, SYNC_CODE_LEN + 2, timeout);
    return res;
}

uint8_t add_sensors_queue_message(const char * conf, uint16_t timeout) {
    return add_timed_message(sensors_queue, conf, SENSORS_CONF_LEN, timeout);
}

uint8_t read_sensors_queue(char * buffer, uint16_t timeout) {
    uint8_t res = read_from_queue (sensors_queue, buffer, SENSORS_CONF_LEN + 1, timeout);
    return res;
}

uint8_t add_heartb_queue_message(const char * conf, uint16_t timeout) {
    return add_timed_message(heartb_queue, conf, 1, timeout);
}

uint8_t read_heartb_queue(char * buffer, uint16_t timeout) {
    uint8_t res = read_from_queue (heartb_queue, buffer, 1, timeout);
    return res;
}
