/*
 * joystick.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad especifica para este proyecto del joystick
 */

#include "joystick.h"

#include "driver/joystick_driver.h"

#define DEAD_ZONE_RADIUS 5500

const uint16_t CENTER[2] = {8000, 8000}; // Centro

// Sector Abajo
const uint16_t DOLE[2] = {13945, 2646}; // Punto Sector Abajo Izquierda
const uint16_t DORI[2] = {13945, 13353}; // Punto Sector Abajo Derecha

// Sector Derecha
const uint16_t RIUP[2] = {7163, 15956}; // Punto Sector Derecha Superior
const uint16_t RIDO[2] = {13353, 13945}; // Punto Sector Derecha Inferior

// Sector Arriba-Derecha
const uint16_t URUP[2] = {1071, 12000}; // Punto Sector Derecha Arriba Superior
const uint16_t URDO[2] = {6336, 15825}; // Punto Sector Derecha Arriba Inferior

// Sector Arriba
const uint16_t UPLE[2] = {691, 4746}; // Punto Sector Arriba Izquierda
const uint16_t UPRI[2] = {691, 11253}; // Punto Sector Arriba Derecha

// Sector Arriba-Izquierda
const uint16_t ULUP[2] = {1071, 4000}; // Punto Sector Izquierda Arriba Inferior
const uint16_t ULDO[2] = {6336, 174}; // Punto Sector Izquierda Arriba Superior

// Sector Izquierda
const uint16_t LEUP[2] = {7163, 43}; // Punto Sector Izquierda Superior
const uint16_t LEDO[2] = {13353, 2054}; // Punto Sector Izquierda Inferior

/*
 * Para comprobar si las coordenadas estan dentro de los sectores primero si el punto se encuentra en sentido horario del vector formado
 * por el centro y el punto que delimitar un sector. De esta forma, el punto se encontrar� en el sector si y solo si esta en sentido
 * horario con uno de los vectores y con el otro no. Adem�s, para comprobar que no sea la zona muerta del centro, se comprobara que el
 * radio sea mayor que el de la zona muerta
 */

/*
 * Funcion para hallar si el punto esta en sentido horario con el vector
 * @param vector Vector con las dos coordenadas del punto de la circunferencia
 * @param x coordenada x del punto a comprobar
 * @param y coordenada y del punto a comprobar
 * @return 1 si se halla en sentido horario con el vector. 0 si no.
 */
static uint8_t are_clockwise(const uint16_t vector[2], uint16_t x, uint16_t y) {
    return (-1 * (vector[0] - CENTER[0]) * (y - CENTER[1]) + (vector[1] - CENTER[1]) * (x - CENTER[0])) > 0;
}

/*
 * Funcion para saber si un punto esta en la zona muerta alrededor del centro
 * @param x coordenada x del punto a comprobar
 * @param y coordenada y del punto a comprobar
 * @return 1 si esta en zona muerta. 0 si no.
 */
static uint8_t is_at_dead_zone(uint16_t x, uint16_t y) {
    // Para calcular si esta en la zona muerta, comprobaremos que la distancia entre el punto y el centro sea menor que el radio
    // de la zona muerta.
    // Para evitar tener que hacer una raiz cuadrada e importar math, utilizaremos todas las medidas al cuadrado
    return (x - CENTER[0])*(x - CENTER[0]) + (y - CENTER[1])*(y - CENTER[1]) <= DEAD_ZONE_RADIUS*DEAD_ZONE_RADIUS;
}

JoystickPosition_t JOYSTICK_get_position(uint16_t x, uint16_t y) {
    JoystickPosition_t res = _JOYSTICK_DEAD_ZONE;

    if (!is_at_dead_zone(x, y)) {

        // Si no esta en zona muerta central, comprobamos en que sector esta
        if (!are_clockwise(DOLE, x, y) && are_clockwise(DORI, x, y)) {
            res = _JOYSTICK_DOWN;
        } else if (!are_clockwise(RIDO, x, y) && are_clockwise(RIUP, x, y)) {
            res = _JOYSTICK_RIGHT;
        } else if (!are_clockwise(URDO, x, y) && are_clockwise(URUP, x, y)) {
            res = _JOYSTICK_UP_RIGHT;
        } else if (!are_clockwise(UPRI, x, y) && are_clockwise(UPLE, x, y)) {
            res = _JOYSTICK_UP;
        } else if (!are_clockwise(ULUP, x, y) && are_clockwise(ULDO, x, y)) {
            res = _JOYSTICK_UP_LEFT;
        } else if (!are_clockwise(LEUP, x, y) && are_clockwise(LEDO, x, y)) {
            res = _JOYSTICK_LEFT;
        }
    }
    return res;
}

void JOYSTICK_read_x_y(uint16_t * x, uint16_t * y) {
    joystick_read(x, y);
}

void JOYSTICK_init() {
    joystick_init();
}
