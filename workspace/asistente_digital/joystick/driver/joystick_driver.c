/*
 * joystick_driver.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad para poder trabajar con el joystick
 */

#include "joystick_driver.h"

/* MSP432 SDK */
#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/adc14.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>

// A15
#define JOYPAD_X_PORT            ( GPIO_PORT_P6 )
#define JOYPAD_X_PIN             ( GPIO_PIN0 )
#define JOYPAD_X_MODE            ( GPIO_TERTIARY_MODULE_FUNCTION ) // Pagina 153 de la datasheet. P6.0 Function A15.

// A9
#define JOYPAD_Y_PORT            ( GPIO_PORT_P4 )
#define JOYPAD_Y_PIN             ( GPIO_PIN4 )
#define JOYPAD_Y_MODE            ( GPIO_TERTIARY_MODULE_FUNCTION ) // Pagina 150 de la datasheet. P4.4 Function A9.


void joystick_init() {
    // Definimos los puertos como perifericos de entrada para el movimiento del Joystick
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(JOYPAD_X_PORT, JOYPAD_X_PIN, JOYPAD_X_MODE);
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(JOYPAD_Y_PORT, JOYPAD_Y_PIN, JOYPAD_Y_MODE);

    // Habilitamos ADC14
    MAP_ADC14_enableModule();

    // Lo iniciamos indicando la senyal de reloj y sus divisores
    MAP_ADC14_initModule(ADC_CLOCKSOURCE_ADCOSC, ADC_PREDIVIDER_64, ADC_DIVIDER_8, 0);

    // Establecemos la configuracion necesaria para A15 y para A9, que guardaremos en MEM4 y MEM5
    MAP_ADC14_configureMultiSequenceMode(ADC_MEM4, ADC_MEM5, true);
    MAP_ADC14_configureConversionMemory(ADC_MEM4, ADC_VREFPOS_AVCC_VREFNEG_VSS, ADC_INPUT_A15, ADC_NONDIFFERENTIAL_INPUTS);
    MAP_ADC14_configureConversionMemory(ADC_MEM5, ADC_VREFPOS_AVCC_VREFNEG_VSS, ADC_INPUT_A9, ADC_NONDIFFERENTIAL_INPUTS);

    // Habilitamos la interrupcion ADC_INT5 de ADC14 para la conversion del joystick
    MAP_ADC14_enableInterrupt(ADC_INT5);

    // Habilitamos la interrupcion de ADC14
    MAP_Interrupt_enableInterrupt(INT_ADC14);

    // Habilitamos que las interrupciones se hagan de forma automatica
    MAP_ADC14_enableSampleTimer(ADC_AUTOMATIC_ITERATION);

    // Comenzamos la conversion Analogica Digital
    MAP_ADC14_enableConversion();
    MAP_ADC14_toggleConversionTrigger();
}

void joystick_read(uint16_t* x, uint16_t* y)
{
   *x = ADC14_getResult(ADC_MEM4);
   *y = ADC14_getResult(ADC_MEM5);
}
