/*
 * joystick.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene definiciones de tipos y funciones necesarias para trabajar con el joystick
 */

#ifndef JOYSTICK_JOYSTICK_H_
#define JOYSTICK_JOYSTICK_H_

#include <stdint.h>


typedef enum {
    _JOYSTICK_DOWN, _JOYSTICK_LEFT, _JOYSTICK_UP_LEFT, _JOYSTICK_UP, _JOYSTICK_UP_RIGHT, _JOYSTICK_RIGHT, _JOYSTICK_DEAD_ZONE
} JoystickPosition_t;


/* Funcion para obtener la posicion del Joystick en funcion de sus coordenadas
 * @param x . Coordenada x
 * @param y. Coordenada y
 * @return Posicion del Joystick
 */
JoystickPosition_t JOYSTICK_get_position(uint16_t x, uint16_t y);

/*
 * Funcion para inicializar el Joystick
 */
void JOYSTICK_init();

/* Funcion para leer los valores x e y del Joystick
 * @param x . Puntero para almacenar la X
 * @param y. Puntero para almacenar la y
 */
void JOYSTICK_read_x_y(uint16_t * x, uint16_t * y);

#endif /* JOYSTICK_JOYSTICK_H_ */
