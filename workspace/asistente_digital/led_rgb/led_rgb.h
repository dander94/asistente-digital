/*
 * led_rgb.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef LED_RGB_LED_RGB_H_
#define LED_RGB_LED_RGB_H_

/*
 * Inicializa el led RGB
 */
void LedRGB_init(void);

/*
 * Apaga el Led
 */
void LedRGB_off(void);

/*
 * Establece el color blanco en el Led
 */
void LedRGB_white(void);

/*
 * Establece el color rojo en el Led
 */
void LedRGB_red(void);

/*
 * Establece el color verde en el Led
 */
void LedRGB_green(void);

/*
 * Establece el color azul en el Led
 */
void LedRGB_blue(void);

/*
 * Establece el color amarillo en el Led
 */
void LedRGB_yellow(void);

/*
 * Establece el color magenta en el Led
 */
void LedRGB_magenta(void);

/*
 * Establece el color cyan en el Led
 */
void LedRGB_cyan(void);


#endif /* LED_RGB_LED_RGB_H_ */
