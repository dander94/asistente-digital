/*
 * led_rgb.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad especifica del LED RGB para este proyecto
 */

#include "led_rgb.h"

#include "driver/led_driver.h"


void LedRGB_off() {
    led_off();
}

void LedRGB_white() {
    led_set_rgb(1, 1, 1);
}

void LedRGB_red() {
    led_set_rgb(1, 0, 0);
}

void LedRGB_green() {
    led_set_rgb(0, 1, 0);
}

void LedRGB_blue() {
    led_set_rgb(0, 0, 1);
}

void LedRGB_yellow() {
    led_set_rgb(1, 1, 0);
}

void LedRGB_magenta() {
    led_set_rgb(1, 0, 1);
}

void LedRGB_cyan() {
    led_set_rgb(0, 1, 1);
}

void LedRGB_init(void)
{
    led_init();
}

