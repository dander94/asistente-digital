/*
 * led_driver.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funconalidad para poder usar el LED RGB del EDU MKII
 */

#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>

/* PINES */
#define RGB_RED_PORT             ( GPIO_PORT_P2 )
#define RGB_RED_PIN              ( GPIO_PIN6 )
#define RGB_GREEN_PORT           ( GPIO_PORT_P2 )
#define RGB_GREEN_PIN            ( GPIO_PIN4 )
#define RGB_BLUE_PORT            ( GPIO_PORT_P5 )
#define RGB_BLUE_PIN             ( GPIO_PIN6 )

void led_off() {
    MAP_GPIO_setOutputLowOnPin(RGB_RED_PORT, RGB_RED_PIN);
    MAP_GPIO_setOutputLowOnPin(RGB_GREEN_PORT, RGB_GREEN_PIN);
    MAP_GPIO_setOutputLowOnPin(RGB_BLUE_PORT, RGB_BLUE_PIN);
}

void led_init(void)
{
    // Inicializamos los puertos y pines que modificaremos para cambiar el color del LED
    MAP_GPIO_setAsOutputPin(RGB_RED_PORT, RGB_RED_PIN);
    MAP_GPIO_setAsOutputPin(RGB_GREEN_PORT, RGB_GREEN_PIN);
    MAP_GPIO_setAsOutputPin(RGB_BLUE_PORT, RGB_BLUE_PIN);

    // Apagamos el LED
    led_off();
}

void led_set_rgb(uint8_t red, uint8_t green, uint8_t blue) {
    if (red == 0) {
        MAP_GPIO_setOutputLowOnPin(RGB_RED_PORT, RGB_RED_PIN);
    } else {
        MAP_GPIO_setOutputHighOnPin(RGB_RED_PORT, RGB_RED_PIN);
    }

    if (green == 0) {
        MAP_GPIO_setOutputLowOnPin(RGB_GREEN_PORT, RGB_GREEN_PIN);
    } else {
        MAP_GPIO_setOutputHighOnPin(RGB_GREEN_PORT, RGB_GREEN_PIN);
    }

    if (blue == 0) {
        MAP_GPIO_setOutputLowOnPin(RGB_BLUE_PORT, RGB_BLUE_PIN);
    } else {
        MAP_GPIO_setOutputHighOnPin(RGB_BLUE_PORT, RGB_BLUE_PIN);
    }

}
