/*
 * pantalla.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Incluye la funcionalidad especifica para usar la pantalla en este proyecto
 */

#include "pantalla.h"

#include <stdint.h>
#include <string.h>

/* Llibreria grafica y drivers del LCD */
#include <ti/grlib/grlib.h>
#include "LcdDriver/Crystalfontz128x128_ST7735.h"

#define FONTS_NUMBER 5

/* Contexto de la pantalla */
static Graphics_Context context;

/*
 * Funcion para limpiar la pantalla
 */
static void clear_screen() {
    // Pintamos toda la pantalla de negro
    Graphics_setForegroundColor(&context, GRAPHICS_COLOR_BLACK);
    Graphics_fillRectangle(&context, &((Graphics_Rectangle) {0, 0, 128, 128}));
    Graphics_setForegroundColor(&context, GRAPHICS_COLOR_YELLOW);
}

void LCD_init() {
    // Inicializamos la pantalla
    Crystalfontz128x128_Init();

    // Cambiamos la orientacion
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_RIGHT);

    // Inicializamos el context de la libreria grafica
    Graphics_initContext(&context, &g_sCrystalfontz128x128, &g_sCrystalfontz128x128_funcs);

    // Establecemos como color de letra el amarillo
    Graphics_setForegroundColor(&context, GRAPHICS_COLOR_YELLOW);

    // Establecemos como color de fondo el negro
    Graphics_setBackgroundColor(&context, GRAPHICS_COLOR_BLACK);

    clear_screen();
}

void LCD_print_sync_code(char * sync_code) {
    uint8_t i;
    char to_draw[3];

    // Limpiamos la pantalla
    clear_screen();

    // Establecemos el tamanyo de la fuente de 34 P
    GrContextFontSet(&context, &g_sFontCmss34b);

    // Pintaremos el texto en tres lineas, para que sea m�s f�cil de leer
    for (i = 0; i < 3; i++) {
        to_draw[0] = sync_code[0 + 3 * i];
        to_draw[1] = sync_code[1 + 3 * i];
        to_draw[2] = sync_code[2 + 3 * i];

        Graphics_drawStringCentered(&context, (int8_t *) to_draw, 3, 64, 16 + 42 * i, OPAQUE_TEXT);
    }
}

void LCD_print_start_sync_mode() {
    clear_screen();

    char msg[] = "SYNC MODE";

    GrContextFontSet(&context, &g_sFontCmss18b);

    Graphics_drawStringCentered(&context, (int8_t *) msg, 9, 64, 64, OPAQUE_TEXT);

}

void LCD_fatal_error(const char * title, const char *msg) {
    clear_screen();

    // Dibujamos la exclamacion de Warning
    struct Graphics_Rectangle rect = {62, 18, 66, 33};

    Graphics_drawLine(&context, 42, 44, 64, 10);
    Graphics_drawLine(&context, 43, 44, 64, 11);
    Graphics_drawLine(&context, 64, 10, 86, 44);
    Graphics_drawLine(&context, 64, 11, 85, 44);
    Graphics_drawLineH(&context, 42, 86, 44);

    Graphics_fillRectangle(&context, &rect);
    Graphics_fillCircle(&context, 64, 38, 2);

    // Establecemos el tamanyo de la fuente de 18 P bold
    GrContextFontSet(&context, &g_sFontCmss16b);
    Graphics_drawStringCentered(&context, (int8_t *) title, strlen(title), 64, 70, OPAQUE_TEXT);

    // Establecemos el tamanyo de la fuente de 10 P
    GrContextFontSet(&context, &g_sFontCmss12);
    Graphics_drawStringCentered(&context, (int8_t *) msg, strlen(msg), 64, 100, OPAQUE_TEXT);
}

void LCD_connecting() {
    clear_screen();

    Graphics_setForegroundColor(&context, GRAPHICS_COLOR_YELLOW);
    Graphics_fillCircle(&context, 32, 50, 8);
    Graphics_fillCircle(&context, 64, 50, 8);
    Graphics_fillCircle(&context, 96, 50, 8);

    GrContextFontSet(&context, &g_sFontCmss16b);
    Graphics_drawStringCentered(&context, "CONECTANDO", 10, 64, 100, OPAQUE_TEXT);
}

void LCD_welcome() {
    clear_screen();

    GrContextFontSet(&context, &g_sFontCmss34b);
    Graphics_drawStringCentered(&context, "HOLA!", 5, 64, 32, OPAQUE_TEXT);

    GrContextFontSet(&context, &g_sFontCmss12);
    Graphics_drawStringCentered(&context, "Todo Listo!", 12, 64, 100, OPAQUE_TEXT);
}
