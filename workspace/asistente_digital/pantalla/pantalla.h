/*
 * pantalla.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PANTALLA_PANTALLA_H_
#define PANTALLA_PANTALLA_H_

/*
 * Funcion para inicializar la pantalla
 */
void LCD_init();

/*
 * Funcion para imprimir el sync code por pantalla
 * @param sync_code
 */
void LCD_print_sync_code(char * sync_code);

/*
 * Funcion para imprimir por pantalla que el dispositivo esta en sync mode
 */
void LCD_print_start_sync_mode();

/*
 * Funcion para mostrar un error fatal por pantalla
 * @param title Titulo del error.
 * @param msg Mensaje del error
 */
void LCD_fatal_error(const char * title, const char * msg);

/*
 * Funcion para mostrar el texto de conectando en la pantalla.
 */
void LCD_connecting();

/*
 * Funcion para mostrar el texto de bienvenida
 */
void LCD_welcome();
#endif /* PANTALLA_PANTALLA_H_ */
