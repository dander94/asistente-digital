/*
 * notification_joystick.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene el hilo de notificaciones desde el joystick
 */


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* MSP SDK */
#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/adc14.h>
#include <ti/devices/msp432p4xx/driverlib/timer_a.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>

/* POSIX QUEUE */
#include "mqueue.h"

/* Configuracion */
#include "configuracion/configuracion.h"

/* Joystick */
#include "joystick/joystick.h"

/* LED RGB */
#include "led_rgb/led_rgb.h"

/* MQTT */
#include "mqtt_client/mqtt_client.h"

/* RTOS */
#include "rtos/rtos.h"

/* Buzzer */
#include "sonido/buzzer.h"

#define JOYSTICK_Q_MSG_SIZE 12

/* Variables globales */
static mqd_t joystick_values_queue;

/* Configuracion del Timer */
static const Timer_A_UpModeConfig timer_config =
{
        TIMER_A_CLOCKSOURCE_ACLK, // 32 Khz
        TIMER_A_CLOCKSOURCE_DIVIDER_64,
        512 * 2,                // 32768 / 64 * 2 segundos
        TIMER_A_TAIE_INTERRUPT_DISABLE,
        TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,
        TIMER_A_DO_CLEAR
};

/*
 * Funcion auxiliar para leer el joystick y serializar su x e y en una string.
 * @param buffer Buffer donde se almacenaran los valores como string
 */
static void serialize_x_y(char * buffer) {
    uint16_t x, y;

    // Leemos en que posicion esta el Joystick
    JOYSTICK_read_x_y(&x, &y);

    // Ponemos primero un 0 indicando que es un valor del joystick, y luego los valores leidos
    sprintf(buffer, "0%05d%05d", x, y);
}

/*
 * Hilo que recibe los valores del joystick y actua segun la posicion.
 * Para recibir los valores usa una cola, donde si el primer valor es indica que es una posicion del Joystick lo que se ha recibido, y si es 1, indica que se debe enviar una notificacion
 * con la ultima posicion registrada
 */
void *notificationJoystickThread(void *arg0) {

    char recv_position[JOYSTICK_Q_MSG_SIZE];
    char recv_x[6];
    char recv_y[6];
    JoystickPosition_t current_position = _JOYSTICK_DEAD_ZONE;
    JoystickPosition_t last_joystick_position = _JOYSTICK_DEAD_ZONE;

    // Esperamos a tener conexion
    while (CONFIG_get_system_status() != _SYS_READY) {
        task_delay(50);
    }

    // Iniciamos la cola

    // Attrs para la queue de heartb.
    // O_NONBLOCK -> No bloqueante si esta llena
    // 10 -> 10 Elementos Maximo
    // JOYSTICK_Q_MSG_SIZE -> Tamanyo de los mensajes
    // 0 -> Mensajes en la cola actualmente
    struct mq_attr joystick_queue_attr = {O_NONBLOCK, 10, JOYSTICK_Q_MSG_SIZE, 0};
    joystick_values_queue = mq_open("joystick_q", O_CREAT | O_RDWR, ( mode_t ) 0, &joystick_queue_attr);

    // Iniciamos el Joystick
    JOYSTICK_init();

    // Iniciamos el LED RGB
    LedRGB_init();

    // Inicializamos el temporizador. Utilizaremos el A3
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A3_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);
    MAP_Timer_A_enableCaptureCompareInterrupt(TIMER_A3_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);

    // Modo Up
    MAP_Timer_A_configureUpMode(TIMER_A3_BASE, &timer_config);
    MAP_Timer_A_clearInterruptFlag(TIMER_A3_BASE);

    // Habilitamos la interrupcion
    MAP_Interrupt_enableInterrupt(INT_TA3_0);

    // Establecemos la prioridad a 1 de las interrupciones para poder usar colas
    MAP_Interrupt_setPriority(INT_ADC14, (4 << 5));
    MAP_Interrupt_setPriority(INT_TA3_0, (2 << 5));

    // Recibimos valores desde la cola del joystick
    while (mq_receive(joystick_values_queue, recv_position, JOYSTICK_Q_MSG_SIZE, 0)) {

        if (recv_position[0] == '0') {
            // Es un valor del joystick

            // Lo parseamos y obtenemos la posicion
            memcpy(recv_x, recv_position + 1, 5);
            memcpy(recv_y, recv_position + 6, 5);
            current_position = JOYSTICK_get_position((uint16_t) atoi(recv_x), (uint16_t) atoi(recv_y));

            // Variamos el LED en funcion de la posicion
           switch (current_position) {
               case _JOYSTICK_DOWN:
                   LedRGB_white();
                   break;
               case _JOYSTICK_RIGHT:
                   LedRGB_cyan();
                   break;
               case _JOYSTICK_UP_RIGHT:
                   LedRGB_magenta();
                   break;
               case _JOYSTICK_UP:
                   LedRGB_blue();
                   break;
               case _JOYSTICK_UP_LEFT:
                   LedRGB_green();
                   break;
               case _JOYSTICK_LEFT:
                   LedRGB_red();
                   break;
               default:
                   LedRGB_off();
                   break;
           }


           if (current_position != last_joystick_position) {
               // Si la posicion no es la mismo que la anterior, paramos el Timer.
               MAP_Timer_A_stopTimer(TIMER_A3_BASE);
               MAP_Timer_A_clearTimer(TIMER_A3_BASE);

               if (current_position != _JOYSTICK_DEAD_ZONE) {
                   // Si no es encuentra en zona muerta, comenzamos el timer
                   MAP_Timer_A_startCounter(TIMER_A3_BASE, TIMER_A_UP_MODE);
               }

               // Guardamos el estado del joystick
               last_joystick_position = current_position;
           }

        } else {
            // Es una notificacion. Se comprueba la posicion y se envia
            switch (current_position) {
                case _JOYSTICK_DOWN:
                    add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_DOWN, 2, 5000);
                    break;
                case _JOYSTICK_RIGHT:
                    add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_RIGHT, 2, 5000);
                    break;
                case _JOYSTICK_UP_RIGHT:
                    add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_UP_RIGHT, 2, 5000);
                    break;
                case _JOYSTICK_UP:
                    add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_UP, 2, 5000);
                    break;
                case _JOYSTICK_UP_LEFT:
                    add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_UP_LEFT, 2, 5000);
                    break;
                case _JOYSTICK_LEFT:
                    add_publish_queue_message(_MQTT_MSG_NOTIF, NOTIFICATION_TYPE_LEFT, 2, 5000);
                    break;
                default:
                    break;
            }
            // Reproducimos el sonido de confirmacion
            BUZZER_play_ack_song(DEVICE_VOLUME);
        }
    }
    while(1){};
}

/*
 * ISR Para el modulo ADC14
 */
void ADC14_IRQHandler(void)
{
    uint64_t status;

    char to_send[JOYSTICK_Q_MSG_SIZE];

    // Recuperamos el estado de la interrupcion
    status = MAP_ADC14_getEnabledInterruptStatus();
    MAP_ADC14_clearInterruptFlag(status);


    // Comprobamos si el joystick es el que ha causado la interrupcion
    if(status & ADC_INT5) {
        // Obtenemos los valores del joystick como string
        serialize_x_y(to_send);
        // Lo enviamos por la cola
        mq_send(joystick_values_queue, to_send, JOYSTICK_Q_MSG_SIZE, 0);
    }
}


/* ISR Para el Timer */
void TA3_0_IRQHandler (void) {
    // Limpiamos la interrupcion y anyadimos la notificacion a la cola de publicar
    MAP_Timer_A_clearInterruptFlag(TIMER_A3_BASE);
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A3_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);
    MAP_Timer_A_stopTimer(TIMER_A3_BASE);

    // Enviamos un 1 para indicar que hay que notificar
    mq_send(joystick_values_queue, "1", 2, 0);
}
