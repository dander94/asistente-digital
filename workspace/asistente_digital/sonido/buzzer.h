/*
 * buzzer.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SONIDO_BUZZER_H_
#define SONIDO_BUZZER_H_

#include <stdint.h>


/*
 * Inicializa el buzzer
 */
void BUZZER_init();

/*
 * Reproduce la cancion de bienvenida.
 */
void BUZZER_play_welcome_song(uint8_t volume);

/*
 * Reproduce la cancion que indica que esta conectado.
 */
void BUZZER_play_connected_song(uint8_t volume);

/*
 * Reproduce la cancion de error.
 */
void BUZZER_play_error_song(uint8_t volume);

/*
 * Reproduce la cancion de confirmacion.
 */
void BUZZER_play_ack_song(uint8_t volume);

#endif /* SONIDO_BUZZER_H_ */
