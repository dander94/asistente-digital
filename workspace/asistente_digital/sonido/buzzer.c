/*
 * buzzer.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad especifica para utilizar el buzzer en este proyecto
 */

#include <sonido/buzzer.h>
#include <sonido/driver/buzzer_driver.h>

/* Duracion de las canciones */
#define WELCOME_SONG_LENGTH     3
#define CONNECTED_SONG_LENGTH   3
#define ERROR_SONG_LENGTH       3
#define ACK_SONG_LENGTH         2

/* Variables globales para la cancion actual */
static tone_t *current_song;
static uint8_t current_song_length;
static uint8_t current_song_next_index;
static uint8_t current_song_volume;

/* Canciones */
static tone_t welcome_song[WELCOME_SONG_LENGTH] = {{140, TONE_C5, NOTE_QUAVER}, {140, TONE_G5, NOTE_QUAVER}, {140, TONE_E5, NOTE_QUAVER}};
static tone_t ack_song[ACK_SONG_LENGTH] = {{140, TONE_C5, NOTE_SEMIQUAVER}, {140, TONE_G5, NOTE_QUAVER}};
static tone_t connected_song[CONNECTED_SONG_LENGTH] = {{140, TONE_G5, NOTE_SEMIQUAVER}, {140, TONE_G5, NOTE_SEMIQUAVER}, {140, TONE_G5, NOTE_QUAVER}};
static tone_t error_song[ERROR_SONG_LENGTH] = {{140, TONE_DS5, NOTE_QUAVER}, {140, TONE_D5, NOTE_QUAVER}, {140, TONE_CS5, NOTE_QUAVER}};

/*
 * Funcion auxiliar para reproducir el siguiente tono de la cancion en curso
 */
static void play_next() {
    if (current_song_next_index < current_song_length){
        // Reproducimos la siguiente nota
        play_tone(*(current_song + current_song_next_index), current_song_volume, (tone_callback_t) play_next);
        current_song_next_index++;
    } else {
        // Si no hay mas notas paramos
        current_song_length = 0;
        current_song_next_index = 0;
    }
}

/*
 * Metodo auxiliar para reproducir una cancion
 * @param song Puntero a un array de tonos
 * @param song_length Numero de notas de la cancion
 * @param volume Volumen al que se debe reproducir la cancion
 */
static void play_song(tone_t * song, uint8_t song_length, uint8_t volume) {
    current_song = song;
    current_song_length = song_length;
    current_song_next_index = 0;
    current_song_volume = volume;

    // Reproducimos el primer tono
    play_next();
}

void BUZZER_play_ack_song(uint8_t volume) {
    play_song(ack_song, ACK_SONG_LENGTH, volume);
}

void BUZZER_play_connected_song(uint8_t volume) {
    play_song(connected_song, CONNECTED_SONG_LENGTH, volume);
}

void BUZZER_play_welcome_song(uint8_t volume) {
    play_song(welcome_song, WELCOME_SONG_LENGTH, volume);
}

void BUZZER_play_error_song(uint8_t volume) {
    play_song(error_song, ERROR_SONG_LENGTH, volume);
}

void BUZZER_init() {
    buzzer_init();
}

