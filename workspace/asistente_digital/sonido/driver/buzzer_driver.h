/*
 * buzzer_driver.h
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SONIDO_DRIVER_BUZZER_DRIVER_H_
#define SONIDO_DRIVER_BUZZER_DRIVER_H_

#include <stdint.h>

/*
 * Notas
 */
#define TONE_C5         523 // DO 5
#define TONE_CS5        554 // DO 5 SOSTENIDO
#define TONE_D5         587 // RE 5
#define TONE_DS5        622 // RE 5 SOSTENIDO
#define TONE_E5         659 // MI 5
#define TONE_F5         698 // FA 5
#define TONE_G5         783 // SOL 5
#define TONE_A5         880 // LA 5
#define TONE_B5         987 // SI 5
#define TONE_C6         1046 // DO 6

/*
 * Tipos de nota
 */
#define NOTE_SEMIBREVE  16 // Redonda
#define NOTE_MINIM      8  // Blanca
#define NOTE_CROTCHET   4  // Negra
#define NOTE_QUAVER     2  // Corchea
#define NOTE_SEMIQUAVER 1  // Semicorchea

// Definimos un tipo propio para el callback. De esta forma, puede ser una variable global configurable.
typedef void (*tone_callback_t)(void);

// Definimos un tipo para un tono completo
typedef struct {
    uint8_t bmp; // Beats por minuto
    uint16_t tone; // Tono
    uint8_t note_type; // Tipo de nota
} tone_t;

/*
 * Metodo para inicializar el buzzer
 */
void buzzer_init();

/*
 * Metodo para reproducir un sonido
 * @param tone. Tono que se va a reproducir. Contiene los BMP, la nota, y el tipo de nota
 * @param volume. Volumen del tono. Entre 1 y 5.
 * @param callback. Funcion que se ejecutara cuando termine el tono
 */
void play_tone(tone_t tone, uint8_t volume, tone_callback_t callback);

#endif /* SONIDO_DRIVER_BUZZER_DRIVER_H_ */
