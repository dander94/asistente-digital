/*
 * buzzer_driver.c
 *
 * Alejandro D�az P�rez. TFG 2020-2021.
 *
 * MIT LICENSE.
 * Copyright 2020 Alejandro D�az
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 *
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Este fichero contiene la funcionalidad para trabajar con el buzzer de la placa EDU MKII
 */
#include <sonido/driver/buzzer_driver.h>

/* MSP 432 SDK */
#include <ti/devices/msp432p4xx/driverlib/rom_map.h>
#include <ti/devices/msp432p4xx/driverlib/timer_a.h>
#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <ti/devices/msp432p4xx/driverlib/interrupt.h>
#include <ti/devices/msp432p4xx/driverlib/cs.h>

// Temporizador para producir el sonido
static Timer_A_PWMConfig pwmConfig = {
    TIMER_A_CLOCKSOURCE_SMCLK, // Utilizaremos el SMCLK ya que al ser un valor m�s grande permitir� obtener mayores frecuencias
    TIMER_A_CLOCKSOURCE_DIVIDER_64,
    0, // Lo ponemos inicialmente a cero ya que cambiaremos el valor dinamicamente.
    TIMER_A_CAPTURECOMPARE_REGISTER_4,
    TIMER_A_OUTPUTMODE_RESET_SET,
    0 // Lo ponemos inicialmente a cero ya que cambiaremos el valor dinamicamente.
};

// Temporizador para la duracion del sonido
static Timer_A_UpModeConfig upConfig =
{
 TIMER_A_CLOCKSOURCE_ACLK, // ACLK sera sufciente para poder calcular la duracion de la nota
 TIMER_A_CLOCKSOURCE_DIVIDER_32,
    0, // Lo ponemos inicialmente a cero ya que cambiaremos el valor dinamicamente.
    TIMER_A_TAIE_INTERRUPT_DISABLE,
    TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,
    TIMER_A_DO_CLEAR
};

// Los divisores del duty cycle determinaran el volumen
static uint8_t duty_cycle_dividers[6] = {1, 100, 50, 25, 10, 2};

// Variable para el callback
static tone_callback_t cur_callback;

/*
 * Devuelve la frecuencia de ACLK entre su divisor
 * @return uint32_t herzios de ackl
 */
static uint32_t get_aclk_hz() {
    return MAP_CS_getACLK() / TIMER_A_CLOCKSOURCE_DIVIDER_32;
}

/*
 * Devuelve la frecuencia de SMCLK entre su divisor
 * @return uint32_t herzio de smclk
 */
static uint32_t get_smclk_hz() {
    return MAP_CS_getSMCLK() / TIMER_A_CLOCKSOURCE_DIVIDER_64;
}

void buzzer_init() {
    // Iniciamos el periferico en el puerto P2.7 en modo primario. (pagina 146 de la datasheet)
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P2, GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION);
}

void play_tone(tone_t tone, uint8_t volume, tone_callback_t callback) {
    // Guardamos el callback como vairbale global.
    cur_callback = callback;

    if (volume > 5 || volume == 0) {
        // Volumen incorrecto.
        return;
    }

    // Definimos cuanto durara el pulso PWM, y por lo tanto la nota
    // Divimos los segundos de un minuto entre el numero de beats por minuto. Multiplicamos por los beats de la nota (1 -> negra)
    // y multiplicamos por los HZ del ACLK
    upConfig.timerPeriod = (uint32_t) ((60.0/tone.bmp) * tone.note_type/4 * get_aclk_hz());

    // Configuramos el Timer con la nueva duracion.
    MAP_Timer_A_configureUpMode(TIMER_A1_BASE, &upConfig);
    MAP_Timer_A_clearInterruptFlag(TIMER_A1_BASE);
    MAP_Timer_A_enableCaptureCompareInterrupt(TIMER_A1_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);
    MAP_Timer_A_startCounter(TIMER_A1_BASE, INT_TA1_0);
    MAP_Interrupt_enableInterrupt(INT_TA1_0);

    // Definimos el PWM, que dara la nota. Para ello dividimos los hz del reloj entre la frecuencia de la nota.
    pwmConfig.timerPeriod = (uint32_t) (get_smclk_hz() / tone.tone);


    // Con el duty cycle definimos el volumen. el 50% de duty cycle dara el m�ximo volumen. La escala de volumen va de 1 a 5
    pwmConfig.dutyCycle   = (uint32_t) (pwmConfig.timerPeriod / duty_cycle_dividers[volume]);

    // Generamos el tono
    Timer_A_generatePWM(TIMER_A0_BASE, &pwmConfig);
}

/*
 * ISR del Timer 1. Se reproducira cuando el tono se haya reproducido.
 */
void TA1_0_IRQHandler(void) {
    // Limpiamos la interrupcion y paramos los contadores
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A1_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0);

    MAP_Timer_A_stopTimer(TIMER_A1_BASE);
    MAP_Timer_A_stopTimer(TIMER_A0_BASE);

    // Ejecutamos el callback
    cur_callback();
}
